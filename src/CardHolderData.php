<?php

namespace GpWebpay\WsApi;

class CardHolderData
{

    /**
     * @var cardholderDetails $cardholderDetails
     */
    protected $cardholderDetails = null;

    /**
     * @var AddressMatchValue $addressMatch
     */
    protected $addressMatch = null;

    /**
     * @var billingDetails $billingDetails
     */
    protected $billingDetails = null;

    /**
     * @var shippingDetails $shippingDetails
     */
    protected $shippingDetails = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return cardholderDetails
     */
    public function getCardholderDetails()
    {
      return $this->cardholderDetails;
    }

    /**
     * @param cardholderDetails $cardholderDetails
     * @return \GpWebpay\WsApi\CardHolderData
     */
    public function setCardholderDetails($cardholderDetails)
    {
      $this->cardholderDetails = $cardholderDetails;
      return $this;
    }

    /**
     * @return AddressMatchValue
     */
    public function getAddressMatch()
    {
      return $this->addressMatch;
    }

    /**
     * @param AddressMatchValue $addressMatch
     * @return \GpWebpay\WsApi\CardHolderData
     */
    public function setAddressMatch($addressMatch)
    {
      $this->addressMatch = $addressMatch;
      return $this;
    }

    /**
     * @return billingDetails
     */
    public function getBillingDetails()
    {
      return $this->billingDetails;
    }

    /**
     * @param billingDetails $billingDetails
     * @return \GpWebpay\WsApi\CardHolderData
     */
    public function setBillingDetails($billingDetails)
    {
      $this->billingDetails = $billingDetails;
      return $this;
    }

    /**
     * @return shippingDetails
     */
    public function getShippingDetails()
    {
      return $this->shippingDetails;
    }

    /**
     * @param shippingDetails $shippingDetails
     * @return \GpWebpay\WsApi\CardHolderData
     */
    public function setShippingDetails($shippingDetails)
    {
      $this->shippingDetails = $shippingDetails;
      return $this;
    }

}
