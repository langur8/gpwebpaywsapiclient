<?php

namespace GpWebpay\WsApi;

class PaymentFaultDetail extends FaultDetail
{

    /**
     * @var AuthCode $authCode
     */
    protected $authCode = null;

    /**
     * @param MessageId $messageId
     * @param ReturnCode $primaryReturnCode
     * @param ReturnCode $secondaryReturnCode
     * @param Signature $signature
     */
    public function __construct($messageId, $primaryReturnCode, $secondaryReturnCode, $signature)
    {
      parent::__construct($messageId, $primaryReturnCode, $secondaryReturnCode, $signature);
    }

    /**
     * @return AuthCode
     */
    public function getAuthCode()
    {
      return $this->authCode;
    }

    /**
     * @param AuthCode $authCode
     * @return \GpWebpay\WsApi\PaymentFaultDetail
     */
    public function setAuthCode($authCode)
    {
      $this->authCode = $authCode;
      return $this;
    }

}
