<?php

namespace GpWebpay\WsApi;

class processTokenPayment
{

    /**
     * @var TokenPaymentRequest $tokenPaymentRequest
     */
    protected $tokenPaymentRequest = null;

    /**
     * @param TokenPaymentRequest $tokenPaymentRequest
     */
    public function __construct($tokenPaymentRequest)
    {
      $this->tokenPaymentRequest = $tokenPaymentRequest;
    }

    /**
     * @return TokenPaymentRequest
     */
    public function getTokenPaymentRequest()
    {
      return $this->tokenPaymentRequest;
    }

    /**
     * @param TokenPaymentRequest $tokenPaymentRequest
     * @return \GpWebpay\WsApi\processTokenPayment
     */
    public function setTokenPaymentRequest($tokenPaymentRequest)
    {
      $this->tokenPaymentRequest = $tokenPaymentRequest;
      return $this;
    }

}
