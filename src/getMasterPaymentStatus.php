<?php

namespace GpWebpay\WsApi;

class getMasterPaymentStatus
{

    /**
     * @var PaymentStatusRequest $masterPaymentStatusRequest
     */
    protected $masterPaymentStatusRequest = null;

    /**
     * @param PaymentStatusRequest $masterPaymentStatusRequest
     */
    public function __construct($masterPaymentStatusRequest)
    {
      $this->masterPaymentStatusRequest = $masterPaymentStatusRequest;
    }

    /**
     * @return PaymentStatusRequest
     */
    public function getMasterPaymentStatusRequest()
    {
      return $this->masterPaymentStatusRequest;
    }

    /**
     * @param PaymentStatusRequest $masterPaymentStatusRequest
     * @return \GpWebpay\WsApi\getMasterPaymentStatus
     */
    public function setMasterPaymentStatusRequest($masterPaymentStatusRequest)
    {
      $this->masterPaymentStatusRequest = $masterPaymentStatusRequest;
      return $this;
    }

}
