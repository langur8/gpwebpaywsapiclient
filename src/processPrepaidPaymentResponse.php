<?php

namespace GpWebpay\WsApi;

class processPrepaidPaymentResponse
{

    /**
     * @var PrepaidPaymentResponse $prepaidPaymentResponse
     */
    protected $prepaidPaymentResponse = null;

    /**
     * @param PrepaidPaymentResponse $prepaidPaymentResponse
     */
    public function __construct($prepaidPaymentResponse)
    {
      $this->prepaidPaymentResponse = $prepaidPaymentResponse;
    }

    /**
     * @return PrepaidPaymentResponse
     */
    public function getPrepaidPaymentResponse()
    {
      return $this->prepaidPaymentResponse;
    }

    /**
     * @param PrepaidPaymentResponse $prepaidPaymentResponse
     * @return \GpWebpay\WsApi\processPrepaidPaymentResponse
     */
    public function setPrepaidPaymentResponse($prepaidPaymentResponse)
    {
      $this->prepaidPaymentResponse = $prepaidPaymentResponse;
      return $this;
    }

}
