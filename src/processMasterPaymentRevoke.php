<?php

namespace GpWebpay\WsApi;

class processMasterPaymentRevoke
{

    /**
     * @var PaymentStatusRequest $masterPaymentStatusRequest
     */
    protected $masterPaymentStatusRequest = null;

    /**
     * @param PaymentStatusRequest $masterPaymentStatusRequest
     */
    public function __construct($masterPaymentStatusRequest)
    {
      $this->masterPaymentStatusRequest = $masterPaymentStatusRequest;
    }

    /**
     * @return PaymentStatusRequest
     */
    public function getMasterPaymentStatusRequest()
    {
      return $this->masterPaymentStatusRequest;
    }

    /**
     * @param PaymentStatusRequest $masterPaymentStatusRequest
     * @return \GpWebpay\WsApi\processMasterPaymentRevoke
     */
    public function setMasterPaymentStatusRequest($masterPaymentStatusRequest)
    {
      $this->masterPaymentStatusRequest = $masterPaymentStatusRequest;
      return $this;
    }

}
