<?php

namespace GpWebpay\WsApi;

class loyaltyProgramDetails
{

    /**
     * @var textValue $programNumber
     */
    protected $programNumber = null;

    /**
     * @var textValue $programId
     */
    protected $programId = null;

    /**
     * @var textValue $programName
     */
    protected $programName = null;

    /**
     * @var month $programExpiryMonth
     */
    protected $programExpiryMonth = null;

    /**
     * @var year $programExpiryYear
     */
    protected $programExpiryYear = null;

    /**
     * @param textValue $programNumber
     * @param textValue $programId
     * @param textValue $programName
     * @param month $programExpiryMonth
     * @param year $programExpiryYear
     */
    public function __construct($programNumber, $programId, $programName, $programExpiryMonth, $programExpiryYear)
    {
      $this->programNumber = $programNumber;
      $this->programId = $programId;
      $this->programName = $programName;
      $this->programExpiryMonth = $programExpiryMonth;
      $this->programExpiryYear = $programExpiryYear;
    }

    /**
     * @return textValue
     */
    public function getProgramNumber()
    {
      return $this->programNumber;
    }

    /**
     * @param textValue $programNumber
     * @return \GpWebpay\WsApi\loyaltyProgramDetails
     */
    public function setProgramNumber($programNumber)
    {
      $this->programNumber = $programNumber;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getProgramId()
    {
      return $this->programId;
    }

    /**
     * @param textValue $programId
     * @return \GpWebpay\WsApi\loyaltyProgramDetails
     */
    public function setProgramId($programId)
    {
      $this->programId = $programId;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getProgramName()
    {
      return $this->programName;
    }

    /**
     * @param textValue $programName
     * @return \GpWebpay\WsApi\loyaltyProgramDetails
     */
    public function setProgramName($programName)
    {
      $this->programName = $programName;
      return $this;
    }

    /**
     * @return month
     */
    public function getProgramExpiryMonth()
    {
      return $this->programExpiryMonth;
    }

    /**
     * @param month $programExpiryMonth
     * @return \GpWebpay\WsApi\loyaltyProgramDetails
     */
    public function setProgramExpiryMonth($programExpiryMonth)
    {
      $this->programExpiryMonth = $programExpiryMonth;
      return $this;
    }

    /**
     * @return year
     */
    public function getProgramExpiryYear()
    {
      return $this->programExpiryYear;
    }

    /**
     * @param year $programExpiryYear
     * @return \GpWebpay\WsApi\loyaltyProgramDetails
     */
    public function setProgramExpiryYear($programExpiryYear)
    {
      $this->programExpiryYear = $programExpiryYear;
      return $this;
    }

}
