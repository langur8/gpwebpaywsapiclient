<?php

namespace GpWebpay\WsApi;

class getPaymentDetail
{

    /**
     * @var PaymentStatusRequest $paymentDetailRequest
     */
    protected $paymentDetailRequest = null;

    /**
     * @param PaymentStatusRequest $paymentDetailRequest
     */
    public function __construct($paymentDetailRequest)
    {
      $this->paymentDetailRequest = $paymentDetailRequest;
    }

    /**
     * @return PaymentStatusRequest
     */
    public function getPaymentDetailRequest()
    {
      return $this->paymentDetailRequest;
    }

    /**
     * @param PaymentStatusRequest $paymentDetailRequest
     * @return \GpWebpay\WsApi\getPaymentDetail
     */
    public function setPaymentDetailRequest($paymentDetailRequest)
    {
      $this->paymentDetailRequest = $paymentDetailRequest;
      return $this;
    }

}
