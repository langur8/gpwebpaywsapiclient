<?php

namespace GpWebpay\WsApi;

class processCaptureReverse
{

    /**
     * @var CaptureReverseRequest $captureReverseRequest
     */
    protected $captureReverseRequest = null;

    /**
     * @param CaptureReverseRequest $captureReverseRequest
     */
    public function __construct($captureReverseRequest)
    {
      $this->captureReverseRequest = $captureReverseRequest;
    }

    /**
     * @return CaptureReverseRequest
     */
    public function getCaptureReverseRequest()
    {
      return $this->captureReverseRequest;
    }

    /**
     * @param CaptureReverseRequest $captureReverseRequest
     * @return \GpWebpay\WsApi\processCaptureReverse
     */
    public function setCaptureReverseRequest($captureReverseRequest)
    {
      $this->captureReverseRequest = $captureReverseRequest;
      return $this;
    }

}
