<?php

namespace GpWebpay\WsApi;

class ShoppingCartItem
{

    /**
     * @var ItemType $itemCode
     */
    protected $itemCode = null;

    /**
     * @var itemDescription $itemDescription
     */
    protected $itemDescription = null;

    /**
     * @var itemQuantity $itemQuantity
     */
    protected $itemQuantity = null;

    /**
     * @var Amount $itemUnitPrice
     */
    protected $itemUnitPrice = null;

    /**
     * @var ItemType $itemClass
     */
    protected $itemClass = null;

    /**
     * @var ItemType $itemType
     */
    protected $itemType = null;

    /**
     * @var itemImageUrl $itemImageUrl
     */
    protected $itemImageUrl = null;

    /**
     * @param itemDescription $itemDescription
     * @param itemQuantity $itemQuantity
     * @param Amount $itemUnitPrice
     */
    public function __construct($itemDescription, $itemQuantity, $itemUnitPrice)
    {
      $this->itemDescription = $itemDescription;
      $this->itemQuantity = $itemQuantity;
      $this->itemUnitPrice = $itemUnitPrice;
    }

    /**
     * @return ItemType
     */
    public function getItemCode()
    {
      return $this->itemCode;
    }

    /**
     * @param ItemType $itemCode
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemCode($itemCode)
    {
      $this->itemCode = $itemCode;
      return $this;
    }

    /**
     * @return itemDescription
     */
    public function getItemDescription()
    {
      return $this->itemDescription;
    }

    /**
     * @param itemDescription $itemDescription
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemDescription($itemDescription)
    {
      $this->itemDescription = $itemDescription;
      return $this;
    }

    /**
     * @return itemQuantity
     */
    public function getItemQuantity()
    {
      return $this->itemQuantity;
    }

    /**
     * @param itemQuantity $itemQuantity
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemQuantity($itemQuantity)
    {
      $this->itemQuantity = $itemQuantity;
      return $this;
    }

    /**
     * @return Amount
     */
    public function getItemUnitPrice()
    {
      return $this->itemUnitPrice;
    }

    /**
     * @param Amount $itemUnitPrice
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemUnitPrice($itemUnitPrice)
    {
      $this->itemUnitPrice = $itemUnitPrice;
      return $this;
    }

    /**
     * @return ItemType
     */
    public function getItemClass()
    {
      return $this->itemClass;
    }

    /**
     * @param ItemType $itemClass
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemClass($itemClass)
    {
      $this->itemClass = $itemClass;
      return $this;
    }

    /**
     * @return ItemType
     */
    public function getItemType()
    {
      return $this->itemType;
    }

    /**
     * @param ItemType $itemType
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemType($itemType)
    {
      $this->itemType = $itemType;
      return $this;
    }

    /**
     * @return itemImageUrl
     */
    public function getItemImageUrl()
    {
      return $this->itemImageUrl;
    }

    /**
     * @param itemImageUrl $itemImageUrl
     * @return \GpWebpay\WsApi\ShoppingCartItem
     */
    public function setItemImageUrl($itemImageUrl)
    {
      $this->itemImageUrl = $itemImageUrl;
      return $this;
    }

}
