<?php

namespace GpWebpay\WsApi;

class getTokenStatus
{

    /**
     * @var TokenStatusRequest $tokenStatusRequest
     */
    protected $tokenStatusRequest = null;

    /**
     * @param TokenStatusRequest $tokenStatusRequest
     */
    public function __construct($tokenStatusRequest)
    {
      $this->tokenStatusRequest = $tokenStatusRequest;
    }

    /**
     * @return TokenStatusRequest
     */
    public function getTokenStatusRequest()
    {
      return $this->tokenStatusRequest;
    }

    /**
     * @param TokenStatusRequest $tokenStatusRequest
     * @return \GpWebpay\WsApi\getTokenStatus
     */
    public function setTokenStatusRequest($tokenStatusRequest)
    {
      $this->tokenStatusRequest = $tokenStatusRequest;
      return $this;
    }

}
