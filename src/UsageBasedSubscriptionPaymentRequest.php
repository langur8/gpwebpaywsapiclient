<?php

namespace GpWebpay\WsApi;

class UsageBasedSubscriptionPaymentRequest
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var Provider $provider
     */
    protected $provider = null;

    /**
     * @var MerchantNumber $merchantNumber
     */
    protected $merchantNumber = null;

    /**
     * @var PaymentNumber $paymentNumber
     */
    protected $paymentNumber = null;

    /**
     * @var PaymentNumber $masterPaymentNumber
     */
    protected $masterPaymentNumber = null;

    /**
     * @var OrderNumber $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var ReferenceNumber $referenceNumber
     */
    protected $referenceNumber = null;

    /**
     * @var Amount $amount
     */
    protected $amount = null;

    /**
     * @var CurrencyCode $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var CaptureFlag $captureFlag
     */
    protected $captureFlag = null;

    /**
     * @var SubMerchantData $subMerchantData
     */
    protected $subMerchantData = null;

    /**
     * @var CardHolderData $cardHolderData
     */
    protected $cardHolderData = null;

    /**
     * @var PaymentInfo $paymentInfo
     */
    protected $paymentInfo = null;

    /**
     * @var ShoppingCartInfo $shoppingCartInfo
     */
    protected $shoppingCartInfo = null;

    /**
     * @var AltTerminalData $altTerminalData
     */
    protected $altTerminalData = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param Provider $provider
     * @param MerchantNumber $merchantNumber
     * @param PaymentNumber $paymentNumber
     * @param PaymentNumber $masterPaymentNumber
     * @param OrderNumber $orderNumber
     * @param ReferenceNumber $referenceNumber
     * @param Amount $amount
     * @param CurrencyCode $currencyCode
     * @param CaptureFlag $captureFlag
     * @param SubMerchantData $subMerchantData
     * @param CardHolderData $cardHolderData
     * @param PaymentInfo $paymentInfo
     * @param ShoppingCartInfo $shoppingCartInfo
     * @param AltTerminalData $altTerminalData
     * @param Signature $signature
     */
    public function __construct($messageId, $provider, $merchantNumber, $paymentNumber, $masterPaymentNumber, $orderNumber, $referenceNumber, $amount, $currencyCode, $captureFlag, $subMerchantData, $cardHolderData, $paymentInfo, $shoppingCartInfo, $altTerminalData, $signature)
    {
      $this->messageId = $messageId;
      $this->provider = $provider;
      $this->merchantNumber = $merchantNumber;
      $this->paymentNumber = $paymentNumber;
      $this->masterPaymentNumber = $masterPaymentNumber;
      $this->orderNumber = $orderNumber;
      $this->referenceNumber = $referenceNumber;
      $this->amount = $amount;
      $this->currencyCode = $currencyCode;
      $this->captureFlag = $captureFlag;
      $this->subMerchantData = $subMerchantData;
      $this->cardHolderData = $cardHolderData;
      $this->paymentInfo = $paymentInfo;
      $this->shoppingCartInfo = $shoppingCartInfo;
      $this->altTerminalData = $altTerminalData;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
      return $this->provider;
    }

    /**
     * @param Provider $provider
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setProvider($provider)
    {
      $this->provider = $provider;
      return $this;
    }

    /**
     * @return MerchantNumber
     */
    public function getMerchantNumber()
    {
      return $this->merchantNumber;
    }

    /**
     * @param MerchantNumber $merchantNumber
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setMerchantNumber($merchantNumber)
    {
      $this->merchantNumber = $merchantNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPaymentNumber()
    {
      return $this->paymentNumber;
    }

    /**
     * @param PaymentNumber $paymentNumber
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setPaymentNumber($paymentNumber)
    {
      $this->paymentNumber = $paymentNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getMasterPaymentNumber()
    {
      return $this->masterPaymentNumber;
    }

    /**
     * @param PaymentNumber $masterPaymentNumber
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setMasterPaymentNumber($masterPaymentNumber)
    {
      $this->masterPaymentNumber = $masterPaymentNumber;
      return $this;
    }

    /**
     * @return OrderNumber
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param OrderNumber $orderNumber
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return ReferenceNumber
     */
    public function getReferenceNumber()
    {
      return $this->referenceNumber;
    }

    /**
     * @param ReferenceNumber $referenceNumber
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setReferenceNumber($referenceNumber)
    {
      $this->referenceNumber = $referenceNumber;
      return $this;
    }

    /**
     * @return Amount
     */
    public function getAmount()
    {
      return $this->amount;
    }

    /**
     * @param Amount $amount
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setAmount($amount)
    {
      $this->amount = $amount;
      return $this;
    }

    /**
     * @return CurrencyCode
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param CurrencyCode $currencyCode
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return CaptureFlag
     */
    public function getCaptureFlag()
    {
      return $this->captureFlag;
    }

    /**
     * @param CaptureFlag $captureFlag
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setCaptureFlag($captureFlag)
    {
      $this->captureFlag = $captureFlag;
      return $this;
    }

    /**
     * @return SubMerchantData
     */
    public function getSubMerchantData()
    {
      return $this->subMerchantData;
    }

    /**
     * @param SubMerchantData $subMerchantData
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setSubMerchantData($subMerchantData)
    {
      $this->subMerchantData = $subMerchantData;
      return $this;
    }

    /**
     * @return CardHolderData
     */
    public function getCardHolderData()
    {
      return $this->cardHolderData;
    }

    /**
     * @param CardHolderData $cardHolderData
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setCardHolderData($cardHolderData)
    {
      $this->cardHolderData = $cardHolderData;
      return $this;
    }

    /**
     * @return PaymentInfo
     */
    public function getPaymentInfo()
    {
      return $this->paymentInfo;
    }

    /**
     * @param PaymentInfo $paymentInfo
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setPaymentInfo($paymentInfo)
    {
      $this->paymentInfo = $paymentInfo;
      return $this;
    }

    /**
     * @return ShoppingCartInfo
     */
    public function getShoppingCartInfo()
    {
      return $this->shoppingCartInfo;
    }

    /**
     * @param ShoppingCartInfo $shoppingCartInfo
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setShoppingCartInfo($shoppingCartInfo)
    {
      $this->shoppingCartInfo = $shoppingCartInfo;
      return $this;
    }

    /**
     * @return AltTerminalData
     */
    public function getAltTerminalData()
    {
      return $this->altTerminalData;
    }

    /**
     * @param AltTerminalData $altTerminalData
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setAltTerminalData($altTerminalData)
    {
      $this->altTerminalData = $altTerminalData;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\UsageBasedSubscriptionPaymentRequest
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
