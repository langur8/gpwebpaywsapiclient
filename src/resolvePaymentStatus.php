<?php

namespace GpWebpay\WsApi;

class resolvePaymentStatus
{

    /**
     * @var ResolvePaymentStatusRequest $resolvePaymentStatusRequest
     */
    protected $resolvePaymentStatusRequest = null;

    /**
     * @param ResolvePaymentStatusRequest $resolvePaymentStatusRequest
     */
    public function __construct($resolvePaymentStatusRequest)
    {
      $this->resolvePaymentStatusRequest = $resolvePaymentStatusRequest;
    }

    /**
     * @return ResolvePaymentStatusRequest
     */
    public function getResolvePaymentStatusRequest()
    {
      return $this->resolvePaymentStatusRequest;
    }

    /**
     * @param ResolvePaymentStatusRequest $resolvePaymentStatusRequest
     * @return \GpWebpay\WsApi\resolvePaymentStatus
     */
    public function setResolvePaymentStatusRequest($resolvePaymentStatusRequest)
    {
      $this->resolvePaymentStatusRequest = $resolvePaymentStatusRequest;
      return $this;
    }

}
