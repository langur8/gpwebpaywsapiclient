<?php

namespace GpWebpay\WsApi;

class PaymentDetailResponse
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var int $state
     */
    protected $state = null;

    /**
     * @var string $status
     */
    protected $status = null;

    /**
     * @var string $subStatus
     */
    protected $subStatus = null;

    /**
     * @var string $paymentMethod
     */
    protected $paymentMethod = null;

    /**
     * @var string $panMasked
     */
    protected $panMasked = null;

    /**
     * @var string $brandName
     */
    protected $brandName = null;

    /**
     * @var int $paymentAmount
     */
    protected $paymentAmount = null;

    /**
     * @var int $approveAmount
     */
    protected $approveAmount = null;

    /**
     * @var int $captureAmount
     */
    protected $captureAmount = null;

    /**
     * @var int $refundAmount
     */
    protected $refundAmount = null;

    /**
     * @var string $approveCode
     */
    protected $approveCode = null;

    /**
     * @var string $paymentTime
     */
    protected $paymentTime = null;

    /**
     * @var string $approveTime
     */
    protected $approveTime = null;

    /**
     * @var string $lastCaptureTime
     */
    protected $lastCaptureTime = null;

    /**
     * @var additionalInfoResponse $additionalInfoResponse
     */
    protected $additionalInfoResponse = null;

    /**
     * @var SimpleValueHolder[] $simpleValueHolder
     */
    protected $simpleValueHolder = null;

    /**
     * @var string $panToken
     */
    protected $panToken = null;

    /**
     * @var string $panPattern
     */
    protected $panPattern = null;

    /**
     * @var string $panExpiry
     */
    protected $panExpiry = null;

    /**
     * @var string $acsResult
     */
    protected $acsResult = null;

    /**
     * @var string $dayToCapture
     */
    protected $dayToCapture = null;

    /**
     * @var TraceId $traceId
     */
    protected $traceId = null;

    /**
     * @var AuthResponseCode $authResponseCode
     */
    protected $authResponseCode = null;

    /**
     * @var AuthRRN $authRRN
     */
    protected $authRRN = null;

    /**
     * @var PaymentAccountReference $paymentAccountReference
     */
    protected $paymentAccountReference = null;

    /**
     * @var string $iasId
     */
    protected $iasId = null;

    /**
     * @var string $payPalId
     */
    protected $payPalId = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param int $state
     * @param Signature $signature
     */
    public function __construct($messageId, $state, $signature)
    {
      $this->messageId = $messageId;
      $this->state = $state;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
      return $this->state;
    }

    /**
     * @param int $state
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setState($state)
    {
      $this->state = $state;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }

    /**
     * @param string $status
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setStatus($status)
    {
      $this->status = $status;
      return $this;
    }

    /**
     * @return string
     */
    public function getSubStatus()
    {
      return $this->subStatus;
    }

    /**
     * @param string $subStatus
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setSubStatus($subStatus)
    {
      $this->subStatus = $subStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
      return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPaymentMethod($paymentMethod)
    {
      $this->paymentMethod = $paymentMethod;
      return $this;
    }

    /**
     * @return string
     */
    public function getPanMasked()
    {
      return $this->panMasked;
    }

    /**
     * @param string $panMasked
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPanMasked($panMasked)
    {
      $this->panMasked = $panMasked;
      return $this;
    }

    /**
     * @return string
     */
    public function getBrandName()
    {
      return $this->brandName;
    }

    /**
     * @param string $brandName
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setBrandName($brandName)
    {
      $this->brandName = $brandName;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaymentAmount()
    {
      return $this->paymentAmount;
    }

    /**
     * @param int $paymentAmount
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPaymentAmount($paymentAmount)
    {
      $this->paymentAmount = $paymentAmount;
      return $this;
    }

    /**
     * @return int
     */
    public function getApproveAmount()
    {
      return $this->approveAmount;
    }

    /**
     * @param int $approveAmount
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setApproveAmount($approveAmount)
    {
      $this->approveAmount = $approveAmount;
      return $this;
    }

    /**
     * @return int
     */
    public function getCaptureAmount()
    {
      return $this->captureAmount;
    }

    /**
     * @param int $captureAmount
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setCaptureAmount($captureAmount)
    {
      $this->captureAmount = $captureAmount;
      return $this;
    }

    /**
     * @return int
     */
    public function getRefundAmount()
    {
      return $this->refundAmount;
    }

    /**
     * @param int $refundAmount
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setRefundAmount($refundAmount)
    {
      $this->refundAmount = $refundAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getApproveCode()
    {
      return $this->approveCode;
    }

    /**
     * @param string $approveCode
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setApproveCode($approveCode)
    {
      $this->approveCode = $approveCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentTime()
    {
      return $this->paymentTime;
    }

    /**
     * @param string $paymentTime
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPaymentTime($paymentTime)
    {
      $this->paymentTime = $paymentTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getApproveTime()
    {
      return $this->approveTime;
    }

    /**
     * @param string $approveTime
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setApproveTime($approveTime)
    {
      $this->approveTime = $approveTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastCaptureTime()
    {
      return $this->lastCaptureTime;
    }

    /**
     * @param string $lastCaptureTime
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setLastCaptureTime($lastCaptureTime)
    {
      $this->lastCaptureTime = $lastCaptureTime;
      return $this;
    }

    /**
     * @return additionalInfoResponse
     */
    public function getAdditionalInfoResponse()
    {
      return $this->additionalInfoResponse;
    }

    /**
     * @param additionalInfoResponse $additionalInfoResponse
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setAdditionalInfoResponse($additionalInfoResponse)
    {
      $this->additionalInfoResponse = $additionalInfoResponse;
      return $this;
    }

    /**
     * @return SimpleValueHolder[]
     */
    public function getSimpleValueHolder()
    {
      return $this->simpleValueHolder;
    }

    /**
     * @param SimpleValueHolder[] $simpleValueHolder
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setSimpleValueHolder(array $simpleValueHolder = null)
    {
      $this->simpleValueHolder = $simpleValueHolder;
      return $this;
    }

    /**
     * @return string
     */
    public function getPanToken()
    {
      return $this->panToken;
    }

    /**
     * @param string $panToken
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPanToken($panToken)
    {
      $this->panToken = $panToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getPanPattern()
    {
      return $this->panPattern;
    }

    /**
     * @param string $panPattern
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPanPattern($panPattern)
    {
      $this->panPattern = $panPattern;
      return $this;
    }

    /**
     * @return string
     */
    public function getPanExpiry()
    {
      return $this->panExpiry;
    }

    /**
     * @param string $panExpiry
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPanExpiry($panExpiry)
    {
      $this->panExpiry = $panExpiry;
      return $this;
    }

    /**
     * @return string
     */
    public function getAcsResult()
    {
      return $this->acsResult;
    }

    /**
     * @param string $acsResult
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setAcsResult($acsResult)
    {
      $this->acsResult = $acsResult;
      return $this;
    }

    /**
     * @return string
     */
    public function getDayToCapture()
    {
      return $this->dayToCapture;
    }

    /**
     * @param string $dayToCapture
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setDayToCapture($dayToCapture)
    {
      $this->dayToCapture = $dayToCapture;
      return $this;
    }

    /**
     * @return TraceId
     */
    public function getTraceId()
    {
      return $this->traceId;
    }

    /**
     * @param TraceId $traceId
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setTraceId($traceId)
    {
      $this->traceId = $traceId;
      return $this;
    }

    /**
     * @return AuthResponseCode
     */
    public function getAuthResponseCode()
    {
      return $this->authResponseCode;
    }

    /**
     * @param AuthResponseCode $authResponseCode
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setAuthResponseCode($authResponseCode)
    {
      $this->authResponseCode = $authResponseCode;
      return $this;
    }

    /**
     * @return AuthRRN
     */
    public function getAuthRRN()
    {
      return $this->authRRN;
    }

    /**
     * @param AuthRRN $authRRN
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setAuthRRN($authRRN)
    {
      $this->authRRN = $authRRN;
      return $this;
    }

    /**
     * @return PaymentAccountReference
     */
    public function getPaymentAccountReference()
    {
      return $this->paymentAccountReference;
    }

    /**
     * @param PaymentAccountReference $paymentAccountReference
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPaymentAccountReference($paymentAccountReference)
    {
      $this->paymentAccountReference = $paymentAccountReference;
      return $this;
    }

    /**
     * @return string
     */
    public function getIasId()
    {
      return $this->iasId;
    }

    /**
     * @param string $iasId
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setIasId($iasId)
    {
      $this->iasId = $iasId;
      return $this;
    }

    /**
     * @return string
     */
    public function getPayPalId()
    {
      return $this->payPalId;
    }

    /**
     * @param string $payPalId
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setPayPalId($payPalId)
    {
      $this->payPalId = $payPalId;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\PaymentDetailResponse
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
