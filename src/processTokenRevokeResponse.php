<?php

namespace GpWebpay\WsApi;

class processTokenRevokeResponse
{

    /**
     * @var TokenRevokeResponse $tokenRevokeResponse
     */
    protected $tokenRevokeResponse = null;

    /**
     * @param TokenRevokeResponse $tokenRevokeResponse
     */
    public function __construct($tokenRevokeResponse)
    {
      $this->tokenRevokeResponse = $tokenRevokeResponse;
    }

    /**
     * @return TokenRevokeResponse
     */
    public function getTokenRevokeResponse()
    {
      return $this->tokenRevokeResponse;
    }

    /**
     * @param TokenRevokeResponse $tokenRevokeResponse
     * @return \GpWebpay\WsApi\processTokenRevokeResponse
     */
    public function setTokenRevokeResponse($tokenRevokeResponse)
    {
      $this->tokenRevokeResponse = $tokenRevokeResponse;
      return $this;
    }

}
