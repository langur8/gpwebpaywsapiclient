<?php

namespace GpWebpay\WsApi;

class processTokenPaymentResponse
{

    /**
     * @var TokenPaymentResponse $tokenPaymentResponse
     */
    protected $tokenPaymentResponse = null;

    /**
     * @param TokenPaymentResponse $tokenPaymentResponse
     */
    public function __construct($tokenPaymentResponse)
    {
      $this->tokenPaymentResponse = $tokenPaymentResponse;
    }

    /**
     * @return TokenPaymentResponse
     */
    public function getTokenPaymentResponse()
    {
      return $this->tokenPaymentResponse;
    }

    /**
     * @param TokenPaymentResponse $tokenPaymentResponse
     * @return \GpWebpay\WsApi\processTokenPaymentResponse
     */
    public function setTokenPaymentResponse($tokenPaymentResponse)
    {
      $this->tokenPaymentResponse = $tokenPaymentResponse;
      return $this;
    }

}
