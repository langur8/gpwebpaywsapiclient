<?php

namespace GpWebpay\WsApi;

class processBatchClose
{

    /**
     * @var BatchCloseRequest $batchClose
     */
    protected $batchClose = null;

    /**
     * @param BatchCloseRequest $batchClose
     */
    public function __construct($batchClose)
    {
      $this->batchClose = $batchClose;
    }

    /**
     * @return BatchCloseRequest
     */
    public function getBatchClose()
    {
      return $this->batchClose;
    }

    /**
     * @param BatchCloseRequest $batchClose
     * @return \GpWebpay\WsApi\processBatchClose
     */
    public function setBatchClose($batchClose)
    {
      $this->batchClose = $batchClose;
      return $this;
    }

}
