<?php

namespace GpWebpay\WsApi;

class processRefundReverse
{

    /**
     * @var RefundReverseRequest $refundReverseRequest
     */
    protected $refundReverseRequest = null;

    /**
     * @param RefundReverseRequest $refundReverseRequest
     */
    public function __construct($refundReverseRequest)
    {
      $this->refundReverseRequest = $refundReverseRequest;
    }

    /**
     * @return RefundReverseRequest
     */
    public function getRefundReverseRequest()
    {
      return $this->refundReverseRequest;
    }

    /**
     * @param RefundReverseRequest $refundReverseRequest
     * @return \GpWebpay\WsApi\processRefundReverse
     */
    public function setRefundReverseRequest($refundReverseRequest)
    {
      $this->refundReverseRequest = $refundReverseRequest;
      return $this;
    }

}
