<?php

namespace GpWebpay\WsApi;

class getTokenStatusResponse
{

    /**
     * @var TokenStatusResponse $tokenStatusResponse
     */
    protected $tokenStatusResponse = null;

    /**
     * @param TokenStatusResponse $tokenStatusResponse
     */
    public function __construct($tokenStatusResponse)
    {
      $this->tokenStatusResponse = $tokenStatusResponse;
    }

    /**
     * @return TokenStatusResponse
     */
    public function getTokenStatusResponse()
    {
      return $this->tokenStatusResponse;
    }

    /**
     * @param TokenStatusResponse $tokenStatusResponse
     * @return \GpWebpay\WsApi\getTokenStatusResponse
     */
    public function setTokenStatusResponse($tokenStatusResponse)
    {
      $this->tokenStatusResponse = $tokenStatusResponse;
      return $this;
    }

}
