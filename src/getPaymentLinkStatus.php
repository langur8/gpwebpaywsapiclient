<?php

namespace GpWebpay\WsApi;

class getPaymentLinkStatus
{

    /**
     * @var PaymentLinkStatusRequest $paymentLinkStatusRequest
     */
    protected $paymentLinkStatusRequest = null;

    /**
     * @param PaymentLinkStatusRequest $paymentLinkStatusRequest
     */
    public function __construct($paymentLinkStatusRequest)
    {
      $this->paymentLinkStatusRequest = $paymentLinkStatusRequest;
    }

    /**
     * @return PaymentLinkStatusRequest
     */
    public function getPaymentLinkStatusRequest()
    {
      return $this->paymentLinkStatusRequest;
    }

    /**
     * @param PaymentLinkStatusRequest $paymentLinkStatusRequest
     * @return \GpWebpay\WsApi\getPaymentLinkStatus
     */
    public function setPaymentLinkStatusRequest($paymentLinkStatusRequest)
    {
      $this->paymentLinkStatusRequest = $paymentLinkStatusRequest;
      return $this;
    }

}
