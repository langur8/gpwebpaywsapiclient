<?php

namespace GpWebpay\WsApi;

class CaptureResponse
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var int $state
     */
    protected $state = null;

    /**
     * @var string $status
     */
    protected $status = null;

    /**
     * @var string $subStatus
     */
    protected $subStatus = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param int $state
     * @param Signature $signature
     */
    public function __construct($messageId, $state, $signature)
    {
      $this->messageId = $messageId;
      $this->state = $state;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\CaptureResponse
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
      return $this->state;
    }

    /**
     * @param int $state
     * @return \GpWebpay\WsApi\CaptureResponse
     */
    public function setState($state)
    {
      $this->state = $state;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }

    /**
     * @param string $status
     * @return \GpWebpay\WsApi\CaptureResponse
     */
    public function setStatus($status)
    {
      $this->status = $status;
      return $this;
    }

    /**
     * @return string
     */
    public function getSubStatus()
    {
      return $this->subStatus;
    }

    /**
     * @param string $subStatus
     * @return \GpWebpay\WsApi\CaptureResponse
     */
    public function setSubStatus($subStatus)
    {
      $this->subStatus = $subStatus;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\CaptureResponse
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
