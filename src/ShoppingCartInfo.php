<?php

namespace GpWebpay\WsApi;

class ShoppingCartInfo
{

    /**
     * @var Amount $taxAmount
     */
    protected $taxAmount = null;

    /**
     * @var Amount12 $shippingAmount
     */
    protected $shippingAmount = null;

    /**
     * @var Amount12 $handlingAmount
     */
    protected $handlingAmount = null;

    /**
     * @var Amount12 $cartAmount
     */
    protected $cartAmount = null;

    /**
     * @var shoppingCartItems $shoppingCartItems
     */
    protected $shoppingCartItems = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Amount
     */
    public function getTaxAmount()
    {
      return $this->taxAmount;
    }

    /**
     * @param Amount $taxAmount
     * @return \GpWebpay\WsApi\ShoppingCartInfo
     */
    public function setTaxAmount($taxAmount)
    {
      $this->taxAmount = $taxAmount;
      return $this;
    }

    /**
     * @return Amount12
     */
    public function getShippingAmount()
    {
      return $this->shippingAmount;
    }

    /**
     * @param Amount12 $shippingAmount
     * @return \GpWebpay\WsApi\ShoppingCartInfo
     */
    public function setShippingAmount($shippingAmount)
    {
      $this->shippingAmount = $shippingAmount;
      return $this;
    }

    /**
     * @return Amount12
     */
    public function getHandlingAmount()
    {
      return $this->handlingAmount;
    }

    /**
     * @param Amount12 $handlingAmount
     * @return \GpWebpay\WsApi\ShoppingCartInfo
     */
    public function setHandlingAmount($handlingAmount)
    {
      $this->handlingAmount = $handlingAmount;
      return $this;
    }

    /**
     * @return Amount12
     */
    public function getCartAmount()
    {
      return $this->cartAmount;
    }

    /**
     * @param Amount12 $cartAmount
     * @return \GpWebpay\WsApi\ShoppingCartInfo
     */
    public function setCartAmount($cartAmount)
    {
      $this->cartAmount = $cartAmount;
      return $this;
    }

    /**
     * @return shoppingCartItems
     */
    public function getShoppingCartItems()
    {
      return $this->shoppingCartItems;
    }

    /**
     * @param shoppingCartItems $shoppingCartItems
     * @return \GpWebpay\WsApi\ShoppingCartInfo
     */
    public function setShoppingCartItems($shoppingCartItems)
    {
      $this->shoppingCartItems = $shoppingCartItems;
      return $this;
    }

}
