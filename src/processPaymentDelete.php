<?php

namespace GpWebpay\WsApi;

class processPaymentDelete
{

    /**
     * @var PaymentDeleteRequest $paymentDeleteRequest
     */
    protected $paymentDeleteRequest = null;

    /**
     * @param PaymentDeleteRequest $paymentDeleteRequest
     */
    public function __construct($paymentDeleteRequest)
    {
      $this->paymentDeleteRequest = $paymentDeleteRequest;
    }

    /**
     * @return PaymentDeleteRequest
     */
    public function getPaymentDeleteRequest()
    {
      return $this->paymentDeleteRequest;
    }

    /**
     * @param PaymentDeleteRequest $paymentDeleteRequest
     * @return \GpWebpay\WsApi\processPaymentDelete
     */
    public function setPaymentDeleteRequest($paymentDeleteRequest)
    {
      $this->paymentDeleteRequest = $paymentDeleteRequest;
      return $this;
    }

}
