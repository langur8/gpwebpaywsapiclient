<?php

namespace GpWebpay\WsApi;

class processAuthorizationReverse
{

    /**
     * @var AuthorizationReverseRequest $authorizationReverseRequest
     */
    protected $authorizationReverseRequest = null;

    /**
     * @param AuthorizationReverseRequest $authorizationReverseRequest
     */
    public function __construct($authorizationReverseRequest)
    {
      $this->authorizationReverseRequest = $authorizationReverseRequest;
    }

    /**
     * @return AuthorizationReverseRequest
     */
    public function getAuthorizationReverseRequest()
    {
      return $this->authorizationReverseRequest;
    }

    /**
     * @param AuthorizationReverseRequest $authorizationReverseRequest
     * @return \GpWebpay\WsApi\processAuthorizationReverse
     */
    public function setAuthorizationReverseRequest($authorizationReverseRequest)
    {
      $this->authorizationReverseRequest = $authorizationReverseRequest;
      return $this;
    }

}
