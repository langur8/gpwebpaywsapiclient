<?php

namespace GpWebpay\WsApi;

class processRefund
{

    /**
     * @var RefundRequest $refundRequest
     */
    protected $refundRequest = null;

    /**
     * @param RefundRequest $refundRequest
     */
    public function __construct($refundRequest)
    {
      $this->refundRequest = $refundRequest;
    }

    /**
     * @return RefundRequest
     */
    public function getRefundRequest()
    {
      return $this->refundRequest;
    }

    /**
     * @param RefundRequest $refundRequest
     * @return \GpWebpay\WsApi\processRefund
     */
    public function setRefundRequest($refundRequest)
    {
      $this->refundRequest = $refundRequest;
      return $this;
    }

}
