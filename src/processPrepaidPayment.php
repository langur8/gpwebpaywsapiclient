<?php

namespace GpWebpay\WsApi;

class processPrepaidPayment
{

    /**
     * @var PrepaidPaymentRequest $prepaidPaymentRequest
     */
    protected $prepaidPaymentRequest = null;

    /**
     * @param PrepaidPaymentRequest $prepaidPaymentRequest
     */
    public function __construct($prepaidPaymentRequest)
    {
      $this->prepaidPaymentRequest = $prepaidPaymentRequest;
    }

    /**
     * @return PrepaidPaymentRequest
     */
    public function getPrepaidPaymentRequest()
    {
      return $this->prepaidPaymentRequest;
    }

    /**
     * @param PrepaidPaymentRequest $prepaidPaymentRequest
     * @return \GpWebpay\WsApi\processPrepaidPayment
     */
    public function setPrepaidPaymentRequest($prepaidPaymentRequest)
    {
      $this->prepaidPaymentRequest = $prepaidPaymentRequest;
      return $this;
    }

}
