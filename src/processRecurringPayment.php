<?php

namespace GpWebpay\WsApi;

class processRecurringPayment
{

    /**
     * @var RecurringPaymentRequest $recurringPaymentRequest
     */
    protected $recurringPaymentRequest = null;

    /**
     * @param RecurringPaymentRequest $recurringPaymentRequest
     */
    public function __construct($recurringPaymentRequest)
    {
      $this->recurringPaymentRequest = $recurringPaymentRequest;
    }

    /**
     * @return RecurringPaymentRequest
     */
    public function getRecurringPaymentRequest()
    {
      return $this->recurringPaymentRequest;
    }

    /**
     * @param RecurringPaymentRequest $recurringPaymentRequest
     * @return \GpWebpay\WsApi\processRecurringPayment
     */
    public function setRecurringPaymentRequest($recurringPaymentRequest)
    {
      $this->recurringPaymentRequest = $recurringPaymentRequest;
      return $this;
    }

}
