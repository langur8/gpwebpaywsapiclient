<?php

namespace GpWebpay\WsApi;

class processRecurringPaymentResponse
{

    /**
     * @var RecurringPaymentResponse $recurringPaymentResponse
     */
    protected $recurringPaymentResponse = null;

    /**
     * @param RecurringPaymentResponse $recurringPaymentResponse
     */
    public function __construct($recurringPaymentResponse)
    {
      $this->recurringPaymentResponse = $recurringPaymentResponse;
    }

    /**
     * @return RecurringPaymentResponse
     */
    public function getRecurringPaymentResponse()
    {
      return $this->recurringPaymentResponse;
    }

    /**
     * @param RecurringPaymentResponse $recurringPaymentResponse
     * @return \GpWebpay\WsApi\processRecurringPaymentResponse
     */
    public function setRecurringPaymentResponse($recurringPaymentResponse)
    {
      $this->recurringPaymentResponse = $recurringPaymentResponse;
      return $this;
    }

}
