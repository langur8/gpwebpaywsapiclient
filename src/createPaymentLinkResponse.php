<?php

namespace GpWebpay\WsApi;

class createPaymentLinkResponse
{

    /**
     * @var PaymentLinkResponse $paymentLinkResponse
     */
    protected $paymentLinkResponse = null;

    /**
     * @param PaymentLinkResponse $paymentLinkResponse
     */
    public function __construct($paymentLinkResponse)
    {
      $this->paymentLinkResponse = $paymentLinkResponse;
    }

    /**
     * @return PaymentLinkResponse
     */
    public function getPaymentLinkResponse()
    {
      return $this->paymentLinkResponse;
    }

    /**
     * @param PaymentLinkResponse $paymentLinkResponse
     * @return \GpWebpay\WsApi\createPaymentLinkResponse
     */
    public function setPaymentLinkResponse($paymentLinkResponse)
    {
      $this->paymentLinkResponse = $paymentLinkResponse;
      return $this;
    }

}
