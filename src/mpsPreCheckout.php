<?php

namespace GpWebpay\WsApi;

class mpsPreCheckout
{

    /**
     * @var MpsPreCheckoutRequest $mpsPreCheckoutRequest
     */
    protected $mpsPreCheckoutRequest = null;

    /**
     * @param MpsPreCheckoutRequest $mpsPreCheckoutRequest
     */
    public function __construct($mpsPreCheckoutRequest)
    {
      $this->mpsPreCheckoutRequest = $mpsPreCheckoutRequest;
    }

    /**
     * @return MpsPreCheckoutRequest
     */
    public function getMpsPreCheckoutRequest()
    {
      return $this->mpsPreCheckoutRequest;
    }

    /**
     * @param MpsPreCheckoutRequest $mpsPreCheckoutRequest
     * @return \GpWebpay\WsApi\mpsPreCheckout
     */
    public function setMpsPreCheckoutRequest($mpsPreCheckoutRequest)
    {
      $this->mpsPreCheckoutRequest = $mpsPreCheckoutRequest;
      return $this;
    }

}
