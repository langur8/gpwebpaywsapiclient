<?php

namespace GpWebpay\WsApi;

class MpsPreCheckoutRewardProgram
{

    /**
     * @var string $rewardNumber
     */
    protected $rewardNumber = null;

    /**
     * @var string $rewardId
     */
    protected $rewardId = null;

    /**
     * @var string $rewardName
     */
    protected $rewardName = null;

    /**
     * @var int $expiryMonth
     */
    protected $expiryMonth = null;

    /**
     * @var int $expiryYear
     */
    protected $expiryYear = null;

    /**
     * @var string $rewardProgramId
     */
    protected $rewardProgramId = null;

    /**
     * @var string $rewardLogoUrl
     */
    protected $rewardLogoUrl = null;

    /**
     * @param string $rewardNumber
     * @param string $rewardId
     * @param string $rewardProgramId
     */
    public function __construct($rewardNumber, $rewardId, $rewardProgramId)
    {
      $this->rewardNumber = $rewardNumber;
      $this->rewardId = $rewardId;
      $this->rewardProgramId = $rewardProgramId;
    }

    /**
     * @return string
     */
    public function getRewardNumber()
    {
      return $this->rewardNumber;
    }

    /**
     * @param string $rewardNumber
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setRewardNumber($rewardNumber)
    {
      $this->rewardNumber = $rewardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRewardId()
    {
      return $this->rewardId;
    }

    /**
     * @param string $rewardId
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setRewardId($rewardId)
    {
      $this->rewardId = $rewardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getRewardName()
    {
      return $this->rewardName;
    }

    /**
     * @param string $rewardName
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setRewardName($rewardName)
    {
      $this->rewardName = $rewardName;
      return $this;
    }

    /**
     * @return int
     */
    public function getExpiryMonth()
    {
      return $this->expiryMonth;
    }

    /**
     * @param int $expiryMonth
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setExpiryMonth($expiryMonth)
    {
      $this->expiryMonth = $expiryMonth;
      return $this;
    }

    /**
     * @return int
     */
    public function getExpiryYear()
    {
      return $this->expiryYear;
    }

    /**
     * @param int $expiryYear
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setExpiryYear($expiryYear)
    {
      $this->expiryYear = $expiryYear;
      return $this;
    }

    /**
     * @return string
     */
    public function getRewardProgramId()
    {
      return $this->rewardProgramId;
    }

    /**
     * @param string $rewardProgramId
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setRewardProgramId($rewardProgramId)
    {
      $this->rewardProgramId = $rewardProgramId;
      return $this;
    }

    /**
     * @return string
     */
    public function getRewardLogoUrl()
    {
      return $this->rewardLogoUrl;
    }

    /**
     * @param string $rewardLogoUrl
     * @return \GpWebpay\WsApi\MpsPreCheckoutRewardProgram
     */
    public function setRewardLogoUrl($rewardLogoUrl)
    {
      $this->rewardLogoUrl = $rewardLogoUrl;
      return $this;
    }

}
