<?php

namespace GpWebpay\WsApi;

class CardOnFilePaymentFaultDetail extends FaultDetail
{

    /**
     * @var string $authenticationLink
     */
    protected $authenticationLink = null;

    /**
     * @param MessageId $messageId
     * @param ReturnCode $primaryReturnCode
     * @param ReturnCode $secondaryReturnCode
     * @param Signature $signature
     */
    public function __construct($messageId, $primaryReturnCode, $secondaryReturnCode, $signature)
    {
      parent::__construct($messageId, $primaryReturnCode, $secondaryReturnCode, $signature);
    }

    /**
     * @return string
     */
    public function getAuthenticationLink()
    {
      return $this->authenticationLink;
    }

    /**
     * @param string $authenticationLink
     * @return \GpWebpay\WsApi\CardOnFilePaymentFaultDetail
     */
    public function setAuthenticationLink($authenticationLink)
    {
      $this->authenticationLink = $authenticationLink;
      return $this;
    }

}
