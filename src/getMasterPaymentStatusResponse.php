<?php

namespace GpWebpay\WsApi;

class getMasterPaymentStatusResponse
{

    /**
     * @var MasterPaymentStatusResponse $masterPaymentStatusResponse
     */
    protected $masterPaymentStatusResponse = null;

    /**
     * @param MasterPaymentStatusResponse $masterPaymentStatusResponse
     */
    public function __construct($masterPaymentStatusResponse)
    {
      $this->masterPaymentStatusResponse = $masterPaymentStatusResponse;
    }

    /**
     * @return MasterPaymentStatusResponse
     */
    public function getMasterPaymentStatusResponse()
    {
      return $this->masterPaymentStatusResponse;
    }

    /**
     * @param MasterPaymentStatusResponse $masterPaymentStatusResponse
     * @return \GpWebpay\WsApi\getMasterPaymentStatusResponse
     */
    public function setMasterPaymentStatusResponse($masterPaymentStatusResponse)
    {
      $this->masterPaymentStatusResponse = $masterPaymentStatusResponse;
      return $this;
    }

}
