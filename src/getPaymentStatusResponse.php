<?php

namespace GpWebpay\WsApi;

class getPaymentStatusResponse
{

    /**
     * @var PaymentStatusResponse $paymentStatusResponse
     */
    protected $paymentStatusResponse = null;

    /**
     * @param PaymentStatusResponse $paymentStatusResponse
     */
    public function __construct($paymentStatusResponse)
    {
      $this->paymentStatusResponse = $paymentStatusResponse;
    }

    /**
     * @return PaymentStatusResponse
     */
    public function getPaymentStatusResponse()
    {
      return $this->paymentStatusResponse;
    }

    /**
     * @param PaymentStatusResponse $paymentStatusResponse
     * @return \GpWebpay\WsApi\getPaymentStatusResponse
     */
    public function setPaymentStatusResponse($paymentStatusResponse)
    {
      $this->paymentStatusResponse = $paymentStatusResponse;
      return $this;
    }

}
