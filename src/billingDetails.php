<?php

namespace GpWebpay\WsApi;

class billingDetails
{

    /**
     * @var TextValue $name
     */
    protected $name = null;

    /**
     * @var AddressValue $address1
     */
    protected $address1 = null;

    /**
     * @var AddressValue $address2
     */
    protected $address2 = null;

    /**
     * @var AddressValue $address3
     */
    protected $address3 = null;

    /**
     * @var CityValue $city
     */
    protected $city = null;

    /**
     * @var PostalCodeValue $postalCode
     */
    protected $postalCode = null;

    /**
     * @var CountryValue $country
     */
    protected $country = null;

    /**
     * @var CountrySubdivisionValue $countrySubdivision
     */
    protected $countrySubdivision = null;

    /**
     * @var PhoneValue $phone
     */
    protected $phone = null;

    /**
     * @var Email $email
     */
    protected $email = null;

    /**
     * @param TextValue $name
     * @param AddressValue $address1
     * @param AddressValue $address2
     * @param AddressValue $address3
     * @param CityValue $city
     * @param PostalCodeValue $postalCode
     * @param CountryValue $country
     * @param CountrySubdivisionValue $countrySubdivision
     * @param PhoneValue $phone
     * @param Email $email
     */
    public function __construct($name, $address1, $address2, $address3, $city, $postalCode, $country, $countrySubdivision, $phone, $email)
    {
      $this->name = $name;
      $this->address1 = $address1;
      $this->address2 = $address2;
      $this->address3 = $address3;
      $this->city = $city;
      $this->postalCode = $postalCode;
      $this->country = $country;
      $this->countrySubdivision = $countrySubdivision;
      $this->phone = $phone;
      $this->email = $email;
    }

    /**
     * @return TextValue
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param TextValue $name
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return AddressValue
     */
    public function getAddress1()
    {
      return $this->address1;
    }

    /**
     * @param AddressValue $address1
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setAddress1($address1)
    {
      $this->address1 = $address1;
      return $this;
    }

    /**
     * @return AddressValue
     */
    public function getAddress2()
    {
      return $this->address2;
    }

    /**
     * @param AddressValue $address2
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setAddress2($address2)
    {
      $this->address2 = $address2;
      return $this;
    }

    /**
     * @return AddressValue
     */
    public function getAddress3()
    {
      return $this->address3;
    }

    /**
     * @param AddressValue $address3
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setAddress3($address3)
    {
      $this->address3 = $address3;
      return $this;
    }

    /**
     * @return CityValue
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param CityValue $city
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return PostalCodeValue
     */
    public function getPostalCode()
    {
      return $this->postalCode;
    }

    /**
     * @param PostalCodeValue $postalCode
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setPostalCode($postalCode)
    {
      $this->postalCode = $postalCode;
      return $this;
    }

    /**
     * @return CountryValue
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param CountryValue $country
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return CountrySubdivisionValue
     */
    public function getCountrySubdivision()
    {
      return $this->countrySubdivision;
    }

    /**
     * @param CountrySubdivisionValue $countrySubdivision
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setCountrySubdivision($countrySubdivision)
    {
      $this->countrySubdivision = $countrySubdivision;
      return $this;
    }

    /**
     * @return PhoneValue
     */
    public function getPhone()
    {
      return $this->phone;
    }

    /**
     * @param PhoneValue $phone
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setPhone($phone)
    {
      $this->phone = $phone;
      return $this;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param Email $email
     * @return \GpWebpay\WsApi\billingDetails
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

}
