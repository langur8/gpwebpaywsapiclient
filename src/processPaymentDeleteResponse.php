<?php

namespace GpWebpay\WsApi;

class processPaymentDeleteResponse
{

    /**
     * @var PaymentDeleteResponse $paymentDeleteResponse
     */
    protected $paymentDeleteResponse = null;

    /**
     * @param PaymentDeleteResponse $paymentDeleteResponse
     */
    public function __construct($paymentDeleteResponse)
    {
      $this->paymentDeleteResponse = $paymentDeleteResponse;
    }

    /**
     * @return PaymentDeleteResponse
     */
    public function getPaymentDeleteResponse()
    {
      return $this->paymentDeleteResponse;
    }

    /**
     * @param PaymentDeleteResponse $paymentDeleteResponse
     * @return \GpWebpay\WsApi\processPaymentDeleteResponse
     */
    public function setPaymentDeleteResponse($paymentDeleteResponse)
    {
      $this->paymentDeleteResponse = $paymentDeleteResponse;
      return $this;
    }

}
