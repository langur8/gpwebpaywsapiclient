<?php

namespace GpWebpay\WsApi;

class FaultDetail
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var ReturnCode $primaryReturnCode
     */
    protected $primaryReturnCode = null;

    /**
     * @var ReturnCode $secondaryReturnCode
     */
    protected $secondaryReturnCode = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param ReturnCode $primaryReturnCode
     * @param ReturnCode $secondaryReturnCode
     * @param Signature $signature
     */
    public function __construct($messageId, $primaryReturnCode, $secondaryReturnCode, $signature)
    {
      $this->messageId = $messageId;
      $this->primaryReturnCode = $primaryReturnCode;
      $this->secondaryReturnCode = $secondaryReturnCode;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\FaultDetail
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return ReturnCode
     */
    public function getPrimaryReturnCode()
    {
      return $this->primaryReturnCode;
    }

    /**
     * @param ReturnCode $primaryReturnCode
     * @return \GpWebpay\WsApi\FaultDetail
     */
    public function setPrimaryReturnCode($primaryReturnCode)
    {
      $this->primaryReturnCode = $primaryReturnCode;
      return $this;
    }

    /**
     * @return ReturnCode
     */
    public function getSecondaryReturnCode()
    {
      return $this->secondaryReturnCode;
    }

    /**
     * @param ReturnCode $secondaryReturnCode
     * @return \GpWebpay\WsApi\FaultDetail
     */
    public function setSecondaryReturnCode($secondaryReturnCode)
    {
      $this->secondaryReturnCode = $secondaryReturnCode;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\FaultDetail
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
