<?php

namespace GpWebpay\WsApi;

class AltTerminalData
{

    /**
     * @var terminalId $terminalId
     */
    protected $terminalId = null;

    /**
     * @var terminalOwner $terminalOwner
     */
    protected $terminalOwner = null;

    /**
     * @var terminalCity $terminalCity
     */
    protected $terminalCity = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return terminalId
     */
    public function getTerminalId()
    {
      return $this->terminalId;
    }

    /**
     * @param terminalId $terminalId
     * @return \GpWebpay\WsApi\AltTerminalData
     */
    public function setTerminalId($terminalId)
    {
      $this->terminalId = $terminalId;
      return $this;
    }

    /**
     * @return terminalOwner
     */
    public function getTerminalOwner()
    {
      return $this->terminalOwner;
    }

    /**
     * @param terminalOwner $terminalOwner
     * @return \GpWebpay\WsApi\AltTerminalData
     */
    public function setTerminalOwner($terminalOwner)
    {
      $this->terminalOwner = $terminalOwner;
      return $this;
    }

    /**
     * @return terminalCity
     */
    public function getTerminalCity()
    {
      return $this->terminalCity;
    }

    /**
     * @param terminalCity $terminalCity
     * @return \GpWebpay\WsApi\AltTerminalData
     */
    public function setTerminalCity($terminalCity)
    {
      $this->terminalCity = $terminalCity;
      return $this;
    }

}
