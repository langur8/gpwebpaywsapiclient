<?php

namespace GpWebpay\WsApi;

class getPaymentDetailResponse
{

    /**
     * @var PaymentDetailResponse $paymentDetailResponse
     */
    protected $paymentDetailResponse = null;

    /**
     * @param PaymentDetailResponse $paymentDetailResponse
     */
    public function __construct($paymentDetailResponse)
    {
      $this->paymentDetailResponse = $paymentDetailResponse;
    }

    /**
     * @return PaymentDetailResponse
     */
    public function getPaymentDetailResponse()
    {
      return $this->paymentDetailResponse;
    }

    /**
     * @param PaymentDetailResponse $paymentDetailResponse
     * @return \GpWebpay\WsApi\getPaymentDetailResponse
     */
    public function setPaymentDetailResponse($paymentDetailResponse)
    {
      $this->paymentDetailResponse = $paymentDetailResponse;
      return $this;
    }

}
