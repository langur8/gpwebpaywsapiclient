<?php

namespace GpWebpay\WsApi;

class processUsageBasedSubscriptionPaymentResponse
{

    /**
     * @var UsageBasedSubscriptionPaymentResponse $usageBasedSubscriptionPaymentResponse
     */
    protected $usageBasedSubscriptionPaymentResponse = null;

    /**
     * @param UsageBasedSubscriptionPaymentResponse $usageBasedSubscriptionPaymentResponse
     */
    public function __construct($usageBasedSubscriptionPaymentResponse)
    {
      $this->usageBasedSubscriptionPaymentResponse = $usageBasedSubscriptionPaymentResponse;
    }

    /**
     * @return UsageBasedSubscriptionPaymentResponse
     */
    public function getUsageBasedSubscriptionPaymentResponse()
    {
      return $this->usageBasedSubscriptionPaymentResponse;
    }

    /**
     * @param UsageBasedSubscriptionPaymentResponse $usageBasedSubscriptionPaymentResponse
     * @return \GpWebpay\WsApi\processUsageBasedSubscriptionPaymentResponse
     */
    public function setUsageBasedSubscriptionPaymentResponse($usageBasedSubscriptionPaymentResponse)
    {
      $this->usageBasedSubscriptionPaymentResponse = $usageBasedSubscriptionPaymentResponse;
      return $this;
    }

}
