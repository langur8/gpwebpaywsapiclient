<?php

namespace GpWebpay\WsApi;

class processRefundReverseResponse
{

    /**
     * @var RefundReverseResponse $refundReverseResponse
     */
    protected $refundReverseResponse = null;

    /**
     * @param RefundReverseResponse $refundReverseResponse
     */
    public function __construct($refundReverseResponse)
    {
      $this->refundReverseResponse = $refundReverseResponse;
    }

    /**
     * @return RefundReverseResponse
     */
    public function getRefundReverseResponse()
    {
      return $this->refundReverseResponse;
    }

    /**
     * @param RefundReverseResponse $refundReverseResponse
     * @return \GpWebpay\WsApi\processRefundReverseResponse
     */
    public function setRefundReverseResponse($refundReverseResponse)
    {
      $this->refundReverseResponse = $refundReverseResponse;
      return $this;
    }

}
