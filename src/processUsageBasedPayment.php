<?php

namespace GpWebpay\WsApi;

class processUsageBasedPayment
{

    /**
     * @var CardOnFilePaymentRequest $usageBasedPaymentRequest
     */
    protected $usageBasedPaymentRequest = null;

    /**
     * @param CardOnFilePaymentRequest $usageBasedPaymentRequest
     */
    public function __construct($usageBasedPaymentRequest)
    {
      $this->usageBasedPaymentRequest = $usageBasedPaymentRequest;
    }

    /**
     * @return CardOnFilePaymentRequest
     */
    public function getUsageBasedPaymentRequest()
    {
      return $this->usageBasedPaymentRequest;
    }

    /**
     * @param CardOnFilePaymentRequest $usageBasedPaymentRequest
     * @return \GpWebpay\WsApi\processUsageBasedPayment
     */
    public function setUsageBasedPaymentRequest($usageBasedPaymentRequest)
    {
      $this->usageBasedPaymentRequest = $usageBasedPaymentRequest;
      return $this;
    }

}
