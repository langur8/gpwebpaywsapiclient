<?php

namespace GpWebpay\WsApi;

class getPaymentStatus
{

    /**
     * @var PaymentStatusRequest $paymentStatusRequest
     */
    protected $paymentStatusRequest = null;

    /**
     * @param PaymentStatusRequest $paymentStatusRequest
     */
    public function __construct($paymentStatusRequest)
    {
      $this->paymentStatusRequest = $paymentStatusRequest;
    }

    /**
     * @return PaymentStatusRequest
     */
    public function getPaymentStatusRequest()
    {
      return $this->paymentStatusRequest;
    }

    /**
     * @param PaymentStatusRequest $paymentStatusRequest
     * @return \GpWebpay\WsApi\getPaymentStatus
     */
    public function setPaymentStatusRequest($paymentStatusRequest)
    {
      $this->paymentStatusRequest = $paymentStatusRequest;
      return $this;
    }

}
