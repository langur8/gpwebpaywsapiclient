<?php

namespace GpWebpay\WsApi;

class MpsPreCheckoutResponse
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var MpsPreCheckoutData $preCheckoutData
     */
    protected $preCheckoutData = null;

    /**
     * @var anyURI $walletPartnerLogoUrl
     */
    protected $walletPartnerLogoUrl = null;

    /**
     * @var anyURI $masterpassLogoUrl
     */
    protected $masterpassLogoUrl = null;

    /**
     * @var SimpleValueHolder[] $simpleValueHolder
     */
    protected $simpleValueHolder = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param MpsPreCheckoutData $preCheckoutData
     * @param anyURI $walletPartnerLogoUrl
     * @param anyURI $masterpassLogoUrl
     * @param Signature $signature
     */
    public function __construct($messageId, $preCheckoutData, $walletPartnerLogoUrl, $masterpassLogoUrl, $signature)
    {
      $this->messageId = $messageId;
      $this->preCheckoutData = $preCheckoutData;
      $this->walletPartnerLogoUrl = $walletPartnerLogoUrl;
      $this->masterpassLogoUrl = $masterpassLogoUrl;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\MpsPreCheckoutResponse
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return MpsPreCheckoutData
     */
    public function getPreCheckoutData()
    {
      return $this->preCheckoutData;
    }

    /**
     * @param MpsPreCheckoutData $preCheckoutData
     * @return \GpWebpay\WsApi\MpsPreCheckoutResponse
     */
    public function setPreCheckoutData($preCheckoutData)
    {
      $this->preCheckoutData = $preCheckoutData;
      return $this;
    }

    /**
     * @return anyURI
     */
    public function getWalletPartnerLogoUrl()
    {
      return $this->walletPartnerLogoUrl;
    }

    /**
     * @param anyURI $walletPartnerLogoUrl
     * @return \GpWebpay\WsApi\MpsPreCheckoutResponse
     */
    public function setWalletPartnerLogoUrl($walletPartnerLogoUrl)
    {
      $this->walletPartnerLogoUrl = $walletPartnerLogoUrl;
      return $this;
    }

    /**
     * @return anyURI
     */
    public function getMasterpassLogoUrl()
    {
      return $this->masterpassLogoUrl;
    }

    /**
     * @param anyURI $masterpassLogoUrl
     * @return \GpWebpay\WsApi\MpsPreCheckoutResponse
     */
    public function setMasterpassLogoUrl($masterpassLogoUrl)
    {
      $this->masterpassLogoUrl = $masterpassLogoUrl;
      return $this;
    }

    /**
     * @return SimpleValueHolder[]
     */
    public function getSimpleValueHolder()
    {
      return $this->simpleValueHolder;
    }

    /**
     * @param SimpleValueHolder[] $simpleValueHolder
     * @return \GpWebpay\WsApi\MpsPreCheckoutResponse
     */
    public function setSimpleValueHolder(array $simpleValueHolder = null)
    {
      $this->simpleValueHolder = $simpleValueHolder;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\MpsPreCheckoutResponse
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
