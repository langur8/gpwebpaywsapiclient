<?php

namespace GpWebpay\WsApi;

class cardholderDetails
{

    /**
     * @var NameValue $name
     */
    protected $name = null;

    /**
     * @var TextValue $loginId
     */
    protected $loginId = null;

    /**
     * @var LoginTypeValue $loginType
     */
    protected $loginType = null;

    /**
     * @var LoginTimeValue $loginTime
     */
    protected $loginTime = null;

    /**
     * @var UserAccountIdValue $userAccountId
     */
    protected $userAccountId = null;

    /**
     * @var DateTypeValue $userAccountCreatedDate
     */
    protected $userAccountCreatedDate = null;

    /**
     * @var UserAccountAgeValue $userAccountAge
     */
    protected $userAccountAge = null;

    /**
     * @var DateTypeValue $userAccountLastChangeDate
     */
    protected $userAccountLastChangeDate = null;

    /**
     * @var UserAccountLastChangeAgeValue $userAccountLastChangeAge
     */
    protected $userAccountLastChangeAge = null;

    /**
     * @var DateTypeValue $userAccountPasswordChangeDate
     */
    protected $userAccountPasswordChangeDate = null;

    /**
     * @var UserAccountPasswordChangeAgeValue $userAccountPasswordChangeAge
     */
    protected $userAccountPasswordChangeAge = null;

    /**
     * @var TextValue $socialNetworkId
     */
    protected $socialNetworkId = null;

    /**
     * @var Email $email
     */
    protected $email = null;

    /**
     * @var PhoneCountryValue $phoneCountry
     */
    protected $phoneCountry = null;

    /**
     * @var PhoneValue $phone
     */
    protected $phone = null;

    /**
     * @var PhoneCountryValue $mobilePhoneCountry
     */
    protected $mobilePhoneCountry = null;

    /**
     * @var PhoneValue $mobilePhone
     */
    protected $mobilePhone = null;

    /**
     * @var PhoneCountryValue $workPhoneCountry
     */
    protected $workPhoneCountry = null;

    /**
     * @var PhoneValue $workPhone
     */
    protected $workPhone = null;

    /**
     * @var IpValue $clientIpAddress
     */
    protected $clientIpAddress = null;

    /**
     * @param NameValue $name
     * @param TextValue $loginId
     * @param LoginTypeValue $loginType
     * @param LoginTimeValue $loginTime
     * @param UserAccountIdValue $userAccountId
     * @param DateTypeValue $userAccountCreatedDate
     * @param UserAccountAgeValue $userAccountAge
     * @param DateTypeValue $userAccountLastChangeDate
     * @param UserAccountLastChangeAgeValue $userAccountLastChangeAge
     * @param DateTypeValue $userAccountPasswordChangeDate
     * @param UserAccountPasswordChangeAgeValue $userAccountPasswordChangeAge
     * @param TextValue $socialNetworkId
     * @param Email $email
     * @param PhoneCountryValue $phoneCountry
     * @param PhoneValue $phone
     * @param PhoneCountryValue $mobilePhoneCountry
     * @param PhoneValue $mobilePhone
     * @param PhoneCountryValue $workPhoneCountry
     * @param PhoneValue $workPhone
     * @param IpValue $clientIpAddress
     */
    public function __construct($name, $loginId, $loginType, $loginTime, $userAccountId, $userAccountCreatedDate, $userAccountAge, $userAccountLastChangeDate, $userAccountLastChangeAge, $userAccountPasswordChangeDate, $userAccountPasswordChangeAge, $socialNetworkId, $email, $phoneCountry, $phone, $mobilePhoneCountry, $mobilePhone, $workPhoneCountry, $workPhone, $clientIpAddress)
    {
      $this->name = $name;
      $this->loginId = $loginId;
      $this->loginType = $loginType;
      $this->loginTime = $loginTime;
      $this->userAccountId = $userAccountId;
      $this->userAccountCreatedDate = $userAccountCreatedDate;
      $this->userAccountAge = $userAccountAge;
      $this->userAccountLastChangeDate = $userAccountLastChangeDate;
      $this->userAccountLastChangeAge = $userAccountLastChangeAge;
      $this->userAccountPasswordChangeDate = $userAccountPasswordChangeDate;
      $this->userAccountPasswordChangeAge = $userAccountPasswordChangeAge;
      $this->socialNetworkId = $socialNetworkId;
      $this->email = $email;
      $this->phoneCountry = $phoneCountry;
      $this->phone = $phone;
      $this->mobilePhoneCountry = $mobilePhoneCountry;
      $this->mobilePhone = $mobilePhone;
      $this->workPhoneCountry = $workPhoneCountry;
      $this->workPhone = $workPhone;
      $this->clientIpAddress = $clientIpAddress;
    }

    /**
     * @return NameValue
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param NameValue $name
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return TextValue
     */
    public function getLoginId()
    {
      return $this->loginId;
    }

    /**
     * @param TextValue $loginId
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setLoginId($loginId)
    {
      $this->loginId = $loginId;
      return $this;
    }

    /**
     * @return LoginTypeValue
     */
    public function getLoginType()
    {
      return $this->loginType;
    }

    /**
     * @param LoginTypeValue $loginType
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setLoginType($loginType)
    {
      $this->loginType = $loginType;
      return $this;
    }

    /**
     * @return LoginTimeValue
     */
    public function getLoginTime()
    {
      return $this->loginTime;
    }

    /**
     * @param LoginTimeValue $loginTime
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setLoginTime($loginTime)
    {
      $this->loginTime = $loginTime;
      return $this;
    }

    /**
     * @return UserAccountIdValue
     */
    public function getUserAccountId()
    {
      return $this->userAccountId;
    }

    /**
     * @param UserAccountIdValue $userAccountId
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountId($userAccountId)
    {
      $this->userAccountId = $userAccountId;
      return $this;
    }

    /**
     * @return DateTypeValue
     */
    public function getUserAccountCreatedDate()
    {
      return $this->userAccountCreatedDate;
    }

    /**
     * @param DateTypeValue $userAccountCreatedDate
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountCreatedDate($userAccountCreatedDate)
    {
      $this->userAccountCreatedDate = $userAccountCreatedDate;
      return $this;
    }

    /**
     * @return UserAccountAgeValue
     */
    public function getUserAccountAge()
    {
      return $this->userAccountAge;
    }

    /**
     * @param UserAccountAgeValue $userAccountAge
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountAge($userAccountAge)
    {
      $this->userAccountAge = $userAccountAge;
      return $this;
    }

    /**
     * @return DateTypeValue
     */
    public function getUserAccountLastChangeDate()
    {
      return $this->userAccountLastChangeDate;
    }

    /**
     * @param DateTypeValue $userAccountLastChangeDate
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountLastChangeDate($userAccountLastChangeDate)
    {
      $this->userAccountLastChangeDate = $userAccountLastChangeDate;
      return $this;
    }

    /**
     * @return UserAccountLastChangeAgeValue
     */
    public function getUserAccountLastChangeAge()
    {
      return $this->userAccountLastChangeAge;
    }

    /**
     * @param UserAccountLastChangeAgeValue $userAccountLastChangeAge
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountLastChangeAge($userAccountLastChangeAge)
    {
      $this->userAccountLastChangeAge = $userAccountLastChangeAge;
      return $this;
    }

    /**
     * @return DateTypeValue
     */
    public function getUserAccountPasswordChangeDate()
    {
      return $this->userAccountPasswordChangeDate;
    }

    /**
     * @param DateTypeValue $userAccountPasswordChangeDate
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountPasswordChangeDate($userAccountPasswordChangeDate)
    {
      $this->userAccountPasswordChangeDate = $userAccountPasswordChangeDate;
      return $this;
    }

    /**
     * @return UserAccountPasswordChangeAgeValue
     */
    public function getUserAccountPasswordChangeAge()
    {
      return $this->userAccountPasswordChangeAge;
    }

    /**
     * @param UserAccountPasswordChangeAgeValue $userAccountPasswordChangeAge
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setUserAccountPasswordChangeAge($userAccountPasswordChangeAge)
    {
      $this->userAccountPasswordChangeAge = $userAccountPasswordChangeAge;
      return $this;
    }

    /**
     * @return TextValue
     */
    public function getSocialNetworkId()
    {
      return $this->socialNetworkId;
    }

    /**
     * @param TextValue $socialNetworkId
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setSocialNetworkId($socialNetworkId)
    {
      $this->socialNetworkId = $socialNetworkId;
      return $this;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param Email $email
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return PhoneCountryValue
     */
    public function getPhoneCountry()
    {
      return $this->phoneCountry;
    }

    /**
     * @param PhoneCountryValue $phoneCountry
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setPhoneCountry($phoneCountry)
    {
      $this->phoneCountry = $phoneCountry;
      return $this;
    }

    /**
     * @return PhoneValue
     */
    public function getPhone()
    {
      return $this->phone;
    }

    /**
     * @param PhoneValue $phone
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setPhone($phone)
    {
      $this->phone = $phone;
      return $this;
    }

    /**
     * @return PhoneCountryValue
     */
    public function getMobilePhoneCountry()
    {
      return $this->mobilePhoneCountry;
    }

    /**
     * @param PhoneCountryValue $mobilePhoneCountry
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setMobilePhoneCountry($mobilePhoneCountry)
    {
      $this->mobilePhoneCountry = $mobilePhoneCountry;
      return $this;
    }

    /**
     * @return PhoneValue
     */
    public function getMobilePhone()
    {
      return $this->mobilePhone;
    }

    /**
     * @param PhoneValue $mobilePhone
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setMobilePhone($mobilePhone)
    {
      $this->mobilePhone = $mobilePhone;
      return $this;
    }

    /**
     * @return PhoneCountryValue
     */
    public function getWorkPhoneCountry()
    {
      return $this->workPhoneCountry;
    }

    /**
     * @param PhoneCountryValue $workPhoneCountry
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setWorkPhoneCountry($workPhoneCountry)
    {
      $this->workPhoneCountry = $workPhoneCountry;
      return $this;
    }

    /**
     * @return PhoneValue
     */
    public function getWorkPhone()
    {
      return $this->workPhone;
    }

    /**
     * @param PhoneValue $workPhone
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setWorkPhone($workPhone)
    {
      $this->workPhone = $workPhone;
      return $this;
    }

    /**
     * @return IpValue
     */
    public function getClientIpAddress()
    {
      return $this->clientIpAddress;
    }

    /**
     * @param IpValue $clientIpAddress
     * @return \GpWebpay\WsApi\cardholderDetails
     */
    public function setClientIpAddress($clientIpAddress)
    {
      $this->clientIpAddress = $clientIpAddress;
      return $this;
    }

}
