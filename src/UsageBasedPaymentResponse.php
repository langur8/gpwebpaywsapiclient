<?php

namespace GpWebpay\WsApi;

class UsageBasedPaymentResponse
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var AuthCode $authCode
     */
    protected $authCode = null;

    /**
     * @var TokenData $tokenData
     */
    protected $tokenData = null;

    /**
     * @var TraceId $traceId
     */
    protected $traceId = null;

    /**
     * @var AuthResponseCode $authResponseCode
     */
    protected $authResponseCode = null;

    /**
     * @var AuthRRN $authRRN
     */
    protected $authRRN = null;

    /**
     * @var PaymentAccountReference $paymentAccountReference
     */
    protected $paymentAccountReference = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param AuthCode $authCode
     * @param TokenData $tokenData
     * @param Signature $signature
     */
    public function __construct($messageId, $authCode, $tokenData, $signature)
    {
      $this->messageId = $messageId;
      $this->authCode = $authCode;
      $this->tokenData = $tokenData;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return AuthCode
     */
    public function getAuthCode()
    {
      return $this->authCode;
    }

    /**
     * @param AuthCode $authCode
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setAuthCode($authCode)
    {
      $this->authCode = $authCode;
      return $this;
    }

    /**
     * @return TokenData
     */
    public function getTokenData()
    {
      return $this->tokenData;
    }

    /**
     * @param TokenData $tokenData
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setTokenData($tokenData)
    {
      $this->tokenData = $tokenData;
      return $this;
    }

    /**
     * @return TraceId
     */
    public function getTraceId()
    {
      return $this->traceId;
    }

    /**
     * @param TraceId $traceId
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setTraceId($traceId)
    {
      $this->traceId = $traceId;
      return $this;
    }

    /**
     * @return AuthResponseCode
     */
    public function getAuthResponseCode()
    {
      return $this->authResponseCode;
    }

    /**
     * @param AuthResponseCode $authResponseCode
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setAuthResponseCode($authResponseCode)
    {
      $this->authResponseCode = $authResponseCode;
      return $this;
    }

    /**
     * @return AuthRRN
     */
    public function getAuthRRN()
    {
      return $this->authRRN;
    }

    /**
     * @param AuthRRN $authRRN
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setAuthRRN($authRRN)
    {
      $this->authRRN = $authRRN;
      return $this;
    }

    /**
     * @return PaymentAccountReference
     */
    public function getPaymentAccountReference()
    {
      return $this->paymentAccountReference;
    }

    /**
     * @param PaymentAccountReference $paymentAccountReference
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setPaymentAccountReference($paymentAccountReference)
    {
      $this->paymentAccountReference = $paymentAccountReference;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\UsageBasedPaymentResponse
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
