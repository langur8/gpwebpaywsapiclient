<?php

namespace GpWebpay\WsApi;

class cardsDetails
{

    /**
     * @var cardDetail $cardDetail
     */
    protected $cardDetail = null;

    /**
     * @param cardDetail $cardDetail
     */
    public function __construct($cardDetail)
    {
      $this->cardDetail = $cardDetail;
    }

    /**
     * @return cardDetail
     */
    public function getCardDetail()
    {
      return $this->cardDetail;
    }

    /**
     * @param cardDetail $cardDetail
     * @return \GpWebpay\WsApi\cardsDetails
     */
    public function setCardDetail($cardDetail)
    {
      $this->cardDetail = $cardDetail;
      return $this;
    }

}
