<?php

namespace GpWebpay\WsApi;

class additionalInfoResponse
{

    /**
     * @var textValue $walletDetails
     */
    protected $walletDetails = null;

    /**
     * @var contact $contact
     */
    protected $contact = null;

    /**
     * @var billingDetails $billingDetails
     */
    protected $billingDetails = null;

    /**
     * @var shippingDetails $shippingDetails
     */
    protected $shippingDetails = null;

    /**
     * @var cardsDetails $cardsDetails
     */
    protected $cardsDetails = null;

    /**
     * @var loyaltyProgramDetails $loyaltyProgramDetails
     */
    protected $loyaltyProgramDetails = null;

    /**
     * @var eetRegistrationData $eetRegistrationData
     */
    protected $eetRegistrationData = null;

    /**
     * @var anonymous9 $version
     */
    protected $version = null;

    /**
     * @param anonymous9 $version
     */
    public function __construct($version)
    {
      $this->version = $version;
    }

    /**
     * @return textValue
     */
    public function getWalletDetails()
    {
      return $this->walletDetails;
    }

    /**
     * @param textValue $walletDetails
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setWalletDetails($walletDetails)
    {
      $this->walletDetails = $walletDetails;
      return $this;
    }

    /**
     * @return contact
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param contact $contact
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return billingDetails
     */
    public function getBillingDetails()
    {
      return $this->billingDetails;
    }

    /**
     * @param billingDetails $billingDetails
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setBillingDetails($billingDetails)
    {
      $this->billingDetails = $billingDetails;
      return $this;
    }

    /**
     * @return shippingDetails
     */
    public function getShippingDetails()
    {
      return $this->shippingDetails;
    }

    /**
     * @param shippingDetails $shippingDetails
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setShippingDetails($shippingDetails)
    {
      $this->shippingDetails = $shippingDetails;
      return $this;
    }

    /**
     * @return cardsDetails
     */
    public function getCardsDetails()
    {
      return $this->cardsDetails;
    }

    /**
     * @param cardsDetails $cardsDetails
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setCardsDetails($cardsDetails)
    {
      $this->cardsDetails = $cardsDetails;
      return $this;
    }

    /**
     * @return loyaltyProgramDetails
     */
    public function getLoyaltyProgramDetails()
    {
      return $this->loyaltyProgramDetails;
    }

    /**
     * @param loyaltyProgramDetails $loyaltyProgramDetails
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setLoyaltyProgramDetails($loyaltyProgramDetails)
    {
      $this->loyaltyProgramDetails = $loyaltyProgramDetails;
      return $this;
    }

    /**
     * @return eetRegistrationData
     */
    public function getEetRegistrationData()
    {
      return $this->eetRegistrationData;
    }

    /**
     * @param eetRegistrationData $eetRegistrationData
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setEetRegistrationData($eetRegistrationData)
    {
      $this->eetRegistrationData = $eetRegistrationData;
      return $this;
    }

    /**
     * @return anonymous9
     */
    public function getVersion()
    {
      return $this->version;
    }

    /**
     * @param anonymous9 $version
     * @return \GpWebpay\WsApi\additionalInfoResponse
     */
    public function setVersion($version)
    {
      $this->version = $version;
      return $this;
    }

}
