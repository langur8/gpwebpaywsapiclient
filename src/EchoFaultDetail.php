<?php

namespace GpWebpay\WsApi;

class EchoFaultDetail
{

    /**
     * @var ReturnCode $primaryReturnCode
     */
    protected $primaryReturnCode = null;

    /**
     * @var ReturnCode $secondaryReturnCode
     */
    protected $secondaryReturnCode = null;

    /**
     * @param ReturnCode $primaryReturnCode
     * @param ReturnCode $secondaryReturnCode
     */
    public function __construct($primaryReturnCode, $secondaryReturnCode)
    {
      $this->primaryReturnCode = $primaryReturnCode;
      $this->secondaryReturnCode = $secondaryReturnCode;
    }

    /**
     * @return ReturnCode
     */
    public function getPrimaryReturnCode()
    {
      return $this->primaryReturnCode;
    }

    /**
     * @param ReturnCode $primaryReturnCode
     * @return \GpWebpay\WsApi\EchoFaultDetail
     */
    public function setPrimaryReturnCode($primaryReturnCode)
    {
      $this->primaryReturnCode = $primaryReturnCode;
      return $this;
    }

    /**
     * @return ReturnCode
     */
    public function getSecondaryReturnCode()
    {
      return $this->secondaryReturnCode;
    }

    /**
     * @param ReturnCode $secondaryReturnCode
     * @return \GpWebpay\WsApi\EchoFaultDetail
     */
    public function setSecondaryReturnCode($secondaryReturnCode)
    {
      $this->secondaryReturnCode = $secondaryReturnCode;
      return $this;
    }

}
