<?php

namespace GpWebpay\WsApi;

class processBatchCloseResponse
{

    /**
     * @var BatchCloseResponse $batchCloseResponse
     */
    protected $batchCloseResponse = null;

    /**
     * @param BatchCloseResponse $batchCloseResponse
     */
    public function __construct($batchCloseResponse)
    {
      $this->batchCloseResponse = $batchCloseResponse;
    }

    /**
     * @return BatchCloseResponse
     */
    public function getBatchCloseResponse()
    {
      return $this->batchCloseResponse;
    }

    /**
     * @param BatchCloseResponse $batchCloseResponse
     * @return \GpWebpay\WsApi\processBatchCloseResponse
     */
    public function setBatchCloseResponse($batchCloseResponse)
    {
      $this->batchCloseResponse = $batchCloseResponse;
      return $this;
    }

}
