<?php

namespace GpWebpay\WsApi;

class contact
{

    /**
     * @var textValue $firstName
     */
    protected $firstName = null;

    /**
     * @var textValue $lastName
     */
    protected $lastName = null;

    /**
     * @var textValue $country
     */
    protected $country = null;

    /**
     * @var phoneValue $phone
     */
    protected $phone = null;

    /**
     * @var emailValue $email
     */
    protected $email = null;

    /**
     * @param textValue $firstName
     * @param textValue $lastName
     * @param textValue $country
     * @param phoneValue $phone
     * @param emailValue $email
     */
    public function __construct($firstName, $lastName, $country, $phone, $email)
    {
      $this->firstName = $firstName;
      $this->lastName = $lastName;
      $this->country = $country;
      $this->phone = $phone;
      $this->email = $email;
    }

    /**
     * @return textValue
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * @param textValue $firstName
     * @return \GpWebpay\WsApi\contact
     */
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param textValue $lastName
     * @return \GpWebpay\WsApi\contact
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param textValue $country
     * @return \GpWebpay\WsApi\contact
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return phoneValue
     */
    public function getPhone()
    {
      return $this->phone;
    }

    /**
     * @param phoneValue $phone
     * @return \GpWebpay\WsApi\contact
     */
    public function setPhone($phone)
    {
      $this->phone = $phone;
      return $this;
    }

    /**
     * @return emailValue
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param emailValue $email
     * @return \GpWebpay\WsApi\contact
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

}
