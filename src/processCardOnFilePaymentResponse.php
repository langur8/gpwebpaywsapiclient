<?php

namespace GpWebpay\WsApi;

class processCardOnFilePaymentResponse
{

    /**
     * @var CardOnFilePaymentResponse $cardOnFilePaymentResponse
     */
    protected $cardOnFilePaymentResponse = null;

    /**
     * @param CardOnFilePaymentResponse $cardOnFilePaymentResponse
     */
    public function __construct($cardOnFilePaymentResponse)
    {
      $this->cardOnFilePaymentResponse = $cardOnFilePaymentResponse;
    }

    /**
     * @return CardOnFilePaymentResponse
     */
    public function getCardOnFilePaymentResponse()
    {
      return $this->cardOnFilePaymentResponse;
    }

    /**
     * @param CardOnFilePaymentResponse $cardOnFilePaymentResponse
     * @return \GpWebpay\WsApi\processCardOnFilePaymentResponse
     */
    public function setCardOnFilePaymentResponse($cardOnFilePaymentResponse)
    {
      $this->cardOnFilePaymentResponse = $cardOnFilePaymentResponse;
      return $this;
    }

}
