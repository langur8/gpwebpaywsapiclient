<?php

namespace GpWebpay\WsApi;

class mpsPreCheckoutResponse
{

    /**
     * @var MpsPreCheckoutResponse $mpsPreCheckoutResponse
     */
    protected $mpsPreCheckoutResponse = null;

    /**
     * @param MpsPreCheckoutResponse $mpsPreCheckoutResponse
     */
    public function __construct($mpsPreCheckoutResponse)
    {
      $this->mpsPreCheckoutResponse = $mpsPreCheckoutResponse;
    }

    /**
     * @return MpsPreCheckoutResponse
     */
    public function getMpsPreCheckoutResponse()
    {
      return $this->mpsPreCheckoutResponse;
    }

    /**
     * @param MpsPreCheckoutResponse $mpsPreCheckoutResponse
     * @return \GpWebpay\WsApi\mpsPreCheckoutResponse
     */
    public function setMpsPreCheckoutResponse($mpsPreCheckoutResponse)
    {
      $this->mpsPreCheckoutResponse = $mpsPreCheckoutResponse;
      return $this;
    }

}
