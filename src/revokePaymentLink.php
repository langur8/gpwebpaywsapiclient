<?php

namespace GpWebpay\WsApi;

class revokePaymentLink
{

    /**
     * @var PaymentStatusRequest $revokePaymentLinkRequest
     */
    protected $revokePaymentLinkRequest = null;

    /**
     * @param PaymentStatusRequest $revokePaymentLinkRequest
     */
    public function __construct($revokePaymentLinkRequest)
    {
      $this->revokePaymentLinkRequest = $revokePaymentLinkRequest;
    }

    /**
     * @return PaymentStatusRequest
     */
    public function getRevokePaymentLinkRequest()
    {
      return $this->revokePaymentLinkRequest;
    }

    /**
     * @param PaymentStatusRequest $revokePaymentLinkRequest
     * @return \GpWebpay\WsApi\revokePaymentLink
     */
    public function setRevokePaymentLinkRequest($revokePaymentLinkRequest)
    {
      $this->revokePaymentLinkRequest = $revokePaymentLinkRequest;
      return $this;
    }

}
