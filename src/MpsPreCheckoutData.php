<?php

namespace GpWebpay\WsApi;

class MpsPreCheckoutData
{

    /**
     * @var MpsPreCheckoutCard[] $cards
     */
    protected $cards = null;

    /**
     * @var MpsContact $contact
     */
    protected $contact = null;

    /**
     * @var MpsPreCheckoutShippingAddress[] $shippingAddresses
     */
    protected $shippingAddresses = null;

    /**
     * @var MpsPreCheckoutRewardProgram[] $rewardPrograms
     */
    protected $rewardPrograms = null;

    /**
     * @var string $walletName
     */
    protected $walletName = null;

    /**
     * @var SimpleValueHolder[] $simpleValueHolder
     */
    protected $simpleValueHolder = null;

    /**
     * @param MpsPreCheckoutCard[] $cards
     * @param MpsPreCheckoutShippingAddress[] $shippingAddresses
     * @param MpsPreCheckoutRewardProgram[] $rewardPrograms
     * @param string $walletName
     */
    public function __construct(array $cards, array $shippingAddresses, array $rewardPrograms, $walletName)
    {
      $this->cards = $cards;
      $this->shippingAddresses = $shippingAddresses;
      $this->rewardPrograms = $rewardPrograms;
      $this->walletName = $walletName;
    }

    /**
     * @return MpsPreCheckoutCard[]
     */
    public function getCards()
    {
      return $this->cards;
    }

    /**
     * @param MpsPreCheckoutCard[] $cards
     * @return \GpWebpay\WsApi\MpsPreCheckoutData
     */
    public function setCards(array $cards)
    {
      $this->cards = $cards;
      return $this;
    }

    /**
     * @return MpsContact
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param MpsContact $contact
     * @return \GpWebpay\WsApi\MpsPreCheckoutData
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return MpsPreCheckoutShippingAddress[]
     */
    public function getShippingAddresses()
    {
      return $this->shippingAddresses;
    }

    /**
     * @param MpsPreCheckoutShippingAddress[] $shippingAddresses
     * @return \GpWebpay\WsApi\MpsPreCheckoutData
     */
    public function setShippingAddresses(array $shippingAddresses)
    {
      $this->shippingAddresses = $shippingAddresses;
      return $this;
    }

    /**
     * @return MpsPreCheckoutRewardProgram[]
     */
    public function getRewardPrograms()
    {
      return $this->rewardPrograms;
    }

    /**
     * @param MpsPreCheckoutRewardProgram[] $rewardPrograms
     * @return \GpWebpay\WsApi\MpsPreCheckoutData
     */
    public function setRewardPrograms(array $rewardPrograms)
    {
      $this->rewardPrograms = $rewardPrograms;
      return $this;
    }

    /**
     * @return string
     */
    public function getWalletName()
    {
      return $this->walletName;
    }

    /**
     * @param string $walletName
     * @return \GpWebpay\WsApi\MpsPreCheckoutData
     */
    public function setWalletName($walletName)
    {
      $this->walletName = $walletName;
      return $this;
    }

    /**
     * @return SimpleValueHolder[]
     */
    public function getSimpleValueHolder()
    {
      return $this->simpleValueHolder;
    }

    /**
     * @param SimpleValueHolder[] $simpleValueHolder
     * @return \GpWebpay\WsApi\MpsPreCheckoutData
     */
    public function setSimpleValueHolder(array $simpleValueHolder = null)
    {
      $this->simpleValueHolder = $simpleValueHolder;
      return $this;
    }

}
