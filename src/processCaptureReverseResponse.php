<?php

namespace GpWebpay\WsApi;

class processCaptureReverseResponse
{

    /**
     * @var CaptureReverseResponse $captureReverseResponse
     */
    protected $captureReverseResponse = null;

    /**
     * @param CaptureReverseResponse $captureReverseResponse
     */
    public function __construct($captureReverseResponse)
    {
      $this->captureReverseResponse = $captureReverseResponse;
    }

    /**
     * @return CaptureReverseResponse
     */
    public function getCaptureReverseResponse()
    {
      return $this->captureReverseResponse;
    }

    /**
     * @param CaptureReverseResponse $captureReverseResponse
     * @return \GpWebpay\WsApi\processCaptureReverseResponse
     */
    public function setCaptureReverseResponse($captureReverseResponse)
    {
      $this->captureReverseResponse = $captureReverseResponse;
      return $this;
    }

}
