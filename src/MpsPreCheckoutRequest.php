<?php

namespace GpWebpay\WsApi;

class MpsPreCheckoutRequest
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var Provider $provider
     */
    protected $provider = null;

    /**
     * @var MerchantNumber $merchantNumber
     */
    protected $merchantNumber = null;

    /**
     * @var PaymentNumber $pairingNumber
     */
    protected $pairingNumber = null;

    /**
     * @var BooleanType $requestCardDetails
     */
    protected $requestCardDetails = null;

    /**
     * @var BooleanType $requestShippingDetails
     */
    protected $requestShippingDetails = null;

    /**
     * @var BooleanType $requestRewardPrograms
     */
    protected $requestRewardPrograms = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param Provider $provider
     * @param MerchantNumber $merchantNumber
     * @param PaymentNumber $pairingNumber
     * @param Signature $signature
     */
    public function __construct($messageId, $provider, $merchantNumber, $pairingNumber, $signature)
    {
      $this->messageId = $messageId;
      $this->provider = $provider;
      $this->merchantNumber = $merchantNumber;
      $this->pairingNumber = $pairingNumber;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
      return $this->provider;
    }

    /**
     * @param Provider $provider
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setProvider($provider)
    {
      $this->provider = $provider;
      return $this;
    }

    /**
     * @return MerchantNumber
     */
    public function getMerchantNumber()
    {
      return $this->merchantNumber;
    }

    /**
     * @param MerchantNumber $merchantNumber
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setMerchantNumber($merchantNumber)
    {
      $this->merchantNumber = $merchantNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPairingNumber()
    {
      return $this->pairingNumber;
    }

    /**
     * @param PaymentNumber $pairingNumber
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setPairingNumber($pairingNumber)
    {
      $this->pairingNumber = $pairingNumber;
      return $this;
    }

    /**
     * @return BooleanType
     */
    public function getRequestCardDetails()
    {
      return $this->requestCardDetails;
    }

    /**
     * @param BooleanType $requestCardDetails
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setRequestCardDetails($requestCardDetails)
    {
      $this->requestCardDetails = $requestCardDetails;
      return $this;
    }

    /**
     * @return BooleanType
     */
    public function getRequestShippingDetails()
    {
      return $this->requestShippingDetails;
    }

    /**
     * @param BooleanType $requestShippingDetails
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setRequestShippingDetails($requestShippingDetails)
    {
      $this->requestShippingDetails = $requestShippingDetails;
      return $this;
    }

    /**
     * @return BooleanType
     */
    public function getRequestRewardPrograms()
    {
      return $this->requestRewardPrograms;
    }

    /**
     * @param BooleanType $requestRewardPrograms
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setRequestRewardPrograms($requestRewardPrograms)
    {
      $this->requestRewardPrograms = $requestRewardPrograms;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\MpsPreCheckoutRequest
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
