<?php

namespace GpWebpay\WsApi;

class processPaymentClose
{

    /**
     * @var PaymentCloseRequest $paymentCloseRequest
     */
    protected $paymentCloseRequest = null;

    /**
     * @param PaymentCloseRequest $paymentCloseRequest
     */
    public function __construct($paymentCloseRequest)
    {
      $this->paymentCloseRequest = $paymentCloseRequest;
    }

    /**
     * @return PaymentCloseRequest
     */
    public function getPaymentCloseRequest()
    {
      return $this->paymentCloseRequest;
    }

    /**
     * @param PaymentCloseRequest $paymentCloseRequest
     * @return \GpWebpay\WsApi\processPaymentClose
     */
    public function setPaymentCloseRequest($paymentCloseRequest)
    {
      $this->paymentCloseRequest = $paymentCloseRequest;
      return $this;
    }

}
