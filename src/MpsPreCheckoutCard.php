<?php

namespace GpWebpay\WsApi;

class MpsPreCheckoutCard
{

    /**
     * @var string $brandId
     */
    protected $brandId = null;

    /**
     * @var string $brandName
     */
    protected $brandName = null;

    /**
     * @var MpsAddress $billingAddress
     */
    protected $billingAddress = null;

    /**
     * @var string $cardHolderName
     */
    protected $cardHolderName = null;

    /**
     * @var int $expiryMonth
     */
    protected $expiryMonth = null;

    /**
     * @var int $expiryYear
     */
    protected $expiryYear = null;

    /**
     * @var string $cardId
     */
    protected $cardId = null;

    /**
     * @var string $lastFour
     */
    protected $lastFour = null;

    /**
     * @var string $cardAlias
     */
    protected $cardAlias = null;

    /**
     * @var boolean $selectedAsDefault
     */
    protected $selectedAsDefault = null;

    /**
     * @param string $brandId
     * @param string $brandName
     * @param string $cardHolderName
     * @param string $cardId
     * @param string $lastFour
     * @param boolean $selectedAsDefault
     */
    public function __construct($brandId, $brandName, $cardHolderName, $cardId, $lastFour, $selectedAsDefault)
    {
      $this->brandId = $brandId;
      $this->brandName = $brandName;
      $this->cardHolderName = $cardHolderName;
      $this->cardId = $cardId;
      $this->lastFour = $lastFour;
      $this->selectedAsDefault = $selectedAsDefault;
    }

    /**
     * @return string
     */
    public function getBrandId()
    {
      return $this->brandId;
    }

    /**
     * @param string $brandId
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setBrandId($brandId)
    {
      $this->brandId = $brandId;
      return $this;
    }

    /**
     * @return string
     */
    public function getBrandName()
    {
      return $this->brandName;
    }

    /**
     * @param string $brandName
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setBrandName($brandName)
    {
      $this->brandName = $brandName;
      return $this;
    }

    /**
     * @return MpsAddress
     */
    public function getBillingAddress()
    {
      return $this->billingAddress;
    }

    /**
     * @param MpsAddress $billingAddress
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setBillingAddress($billingAddress)
    {
      $this->billingAddress = $billingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardHolderName()
    {
      return $this->cardHolderName;
    }

    /**
     * @param string $cardHolderName
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setCardHolderName($cardHolderName)
    {
      $this->cardHolderName = $cardHolderName;
      return $this;
    }

    /**
     * @return int
     */
    public function getExpiryMonth()
    {
      return $this->expiryMonth;
    }

    /**
     * @param int $expiryMonth
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setExpiryMonth($expiryMonth)
    {
      $this->expiryMonth = $expiryMonth;
      return $this;
    }

    /**
     * @return int
     */
    public function getExpiryYear()
    {
      return $this->expiryYear;
    }

    /**
     * @param int $expiryYear
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setExpiryYear($expiryYear)
    {
      $this->expiryYear = $expiryYear;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardId()
    {
      return $this->cardId;
    }

    /**
     * @param string $cardId
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setCardId($cardId)
    {
      $this->cardId = $cardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastFour()
    {
      return $this->lastFour;
    }

    /**
     * @param string $lastFour
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setLastFour($lastFour)
    {
      $this->lastFour = $lastFour;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardAlias()
    {
      return $this->cardAlias;
    }

    /**
     * @param string $cardAlias
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setCardAlias($cardAlias)
    {
      $this->cardAlias = $cardAlias;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSelectedAsDefault()
    {
      return $this->selectedAsDefault;
    }

    /**
     * @param boolean $selectedAsDefault
     * @return \GpWebpay\WsApi\MpsPreCheckoutCard
     */
    public function setSelectedAsDefault($selectedAsDefault)
    {
      $this->selectedAsDefault = $selectedAsDefault;
      return $this;
    }

}
