<?php

namespace GpWebpay\WsApi;

class SubMerchantData
{

    /**
     * @var merchantId $merchantId
     */
    protected $merchantId = null;

    /**
     * @var merchantType $merchantType
     */
    protected $merchantType = null;

    /**
     * @var merchantName $merchantName
     */
    protected $merchantName = null;

    /**
     * @var merchantStreet $merchantStreet
     */
    protected $merchantStreet = null;

    /**
     * @var merchantCity $merchantCity
     */
    protected $merchantCity = null;

    /**
     * @var merchantPostalCode $merchantPostalCode
     */
    protected $merchantPostalCode = null;

    /**
     * @var merchantState $merchantState
     */
    protected $merchantState = null;

    /**
     * @var merchantCountry $merchantCountry
     */
    protected $merchantCountry = null;

    /**
     * @var merchantWeb $merchantWeb
     */
    protected $merchantWeb = null;

    /**
     * @var merchantServiceNumber $merchantServiceNumber
     */
    protected $merchantServiceNumber = null;

    /**
     * @var merchantMcAssignedId $merchantMcAssignedId
     */
    protected $merchantMcAssignedId = null;

    /**
     * @param merchantId $merchantId
     * @param merchantType $merchantType
     * @param merchantName $merchantName
     * @param merchantStreet $merchantStreet
     * @param merchantCity $merchantCity
     * @param merchantPostalCode $merchantPostalCode
     * @param merchantCountry $merchantCountry
     * @param merchantWeb $merchantWeb
     * @param merchantServiceNumber $merchantServiceNumber
     */
    public function __construct($merchantId, $merchantType, $merchantName, $merchantStreet, $merchantCity, $merchantPostalCode, $merchantCountry, $merchantWeb, $merchantServiceNumber)
    {
      $this->merchantId = $merchantId;
      $this->merchantType = $merchantType;
      $this->merchantName = $merchantName;
      $this->merchantStreet = $merchantStreet;
      $this->merchantCity = $merchantCity;
      $this->merchantPostalCode = $merchantPostalCode;
      $this->merchantCountry = $merchantCountry;
      $this->merchantWeb = $merchantWeb;
      $this->merchantServiceNumber = $merchantServiceNumber;
    }

    /**
     * @return merchantId
     */
    public function getMerchantId()
    {
      return $this->merchantId;
    }

    /**
     * @param merchantId $merchantId
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantId($merchantId)
    {
      $this->merchantId = $merchantId;
      return $this;
    }

    /**
     * @return merchantType
     */
    public function getMerchantType()
    {
      return $this->merchantType;
    }

    /**
     * @param merchantType $merchantType
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantType($merchantType)
    {
      $this->merchantType = $merchantType;
      return $this;
    }

    /**
     * @return merchantName
     */
    public function getMerchantName()
    {
      return $this->merchantName;
    }

    /**
     * @param merchantName $merchantName
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantName($merchantName)
    {
      $this->merchantName = $merchantName;
      return $this;
    }

    /**
     * @return merchantStreet
     */
    public function getMerchantStreet()
    {
      return $this->merchantStreet;
    }

    /**
     * @param merchantStreet $merchantStreet
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantStreet($merchantStreet)
    {
      $this->merchantStreet = $merchantStreet;
      return $this;
    }

    /**
     * @return merchantCity
     */
    public function getMerchantCity()
    {
      return $this->merchantCity;
    }

    /**
     * @param merchantCity $merchantCity
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantCity($merchantCity)
    {
      $this->merchantCity = $merchantCity;
      return $this;
    }

    /**
     * @return merchantPostalCode
     */
    public function getMerchantPostalCode()
    {
      return $this->merchantPostalCode;
    }

    /**
     * @param merchantPostalCode $merchantPostalCode
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantPostalCode($merchantPostalCode)
    {
      $this->merchantPostalCode = $merchantPostalCode;
      return $this;
    }

    /**
     * @return merchantState
     */
    public function getMerchantState()
    {
      return $this->merchantState;
    }

    /**
     * @param merchantState $merchantState
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantState($merchantState)
    {
      $this->merchantState = $merchantState;
      return $this;
    }

    /**
     * @return merchantCountry
     */
    public function getMerchantCountry()
    {
      return $this->merchantCountry;
    }

    /**
     * @param merchantCountry $merchantCountry
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantCountry($merchantCountry)
    {
      $this->merchantCountry = $merchantCountry;
      return $this;
    }

    /**
     * @return merchantWeb
     */
    public function getMerchantWeb()
    {
      return $this->merchantWeb;
    }

    /**
     * @param merchantWeb $merchantWeb
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantWeb($merchantWeb)
    {
      $this->merchantWeb = $merchantWeb;
      return $this;
    }

    /**
     * @return merchantServiceNumber
     */
    public function getMerchantServiceNumber()
    {
      return $this->merchantServiceNumber;
    }

    /**
     * @param merchantServiceNumber $merchantServiceNumber
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantServiceNumber($merchantServiceNumber)
    {
      $this->merchantServiceNumber = $merchantServiceNumber;
      return $this;
    }

    /**
     * @return merchantMcAssignedId
     */
    public function getMerchantMcAssignedId()
    {
      return $this->merchantMcAssignedId;
    }

    /**
     * @param merchantMcAssignedId $merchantMcAssignedId
     * @return \GpWebpay\WsApi\SubMerchantData
     */
    public function setMerchantMcAssignedId($merchantMcAssignedId)
    {
      $this->merchantMcAssignedId = $merchantMcAssignedId;
      return $this;
    }

}
