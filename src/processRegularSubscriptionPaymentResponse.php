<?php

namespace GpWebpay\WsApi;

class processRegularSubscriptionPaymentResponse
{

    /**
     * @var RegularSubscriptionPaymentResponse $regularSubscriptionPaymentResponse
     */
    protected $regularSubscriptionPaymentResponse = null;

    /**
     * @param RegularSubscriptionPaymentResponse $regularSubscriptionPaymentResponse
     */
    public function __construct($regularSubscriptionPaymentResponse)
    {
      $this->regularSubscriptionPaymentResponse = $regularSubscriptionPaymentResponse;
    }

    /**
     * @return RegularSubscriptionPaymentResponse
     */
    public function getRegularSubscriptionPaymentResponse()
    {
      return $this->regularSubscriptionPaymentResponse;
    }

    /**
     * @param RegularSubscriptionPaymentResponse $regularSubscriptionPaymentResponse
     * @return \GpWebpay\WsApi\processRegularSubscriptionPaymentResponse
     */
    public function setRegularSubscriptionPaymentResponse($regularSubscriptionPaymentResponse)
    {
      $this->regularSubscriptionPaymentResponse = $regularSubscriptionPaymentResponse;
      return $this;
    }

}
