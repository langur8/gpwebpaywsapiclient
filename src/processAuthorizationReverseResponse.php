<?php

namespace GpWebpay\WsApi;

class processAuthorizationReverseResponse
{

    /**
     * @var AuthorizationReverseResponse $authorizationReverseResponse
     */
    protected $authorizationReverseResponse = null;

    /**
     * @param AuthorizationReverseResponse $authorizationReverseResponse
     */
    public function __construct($authorizationReverseResponse)
    {
      $this->authorizationReverseResponse = $authorizationReverseResponse;
    }

    /**
     * @return AuthorizationReverseResponse
     */
    public function getAuthorizationReverseResponse()
    {
      return $this->authorizationReverseResponse;
    }

    /**
     * @param AuthorizationReverseResponse $authorizationReverseResponse
     * @return \GpWebpay\WsApi\processAuthorizationReverseResponse
     */
    public function setAuthorizationReverseResponse($authorizationReverseResponse)
    {
      $this->authorizationReverseResponse = $authorizationReverseResponse;
      return $this;
    }

}
