<?php

namespace GpWebpay\WsApi;

class MpsExpressCheckoutResponse
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var AuthCode $authCode
     */
    protected $authCode = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param AuthCode $authCode
     * @param Signature $signature
     */
    public function __construct($messageId, $authCode, $signature)
    {
      $this->messageId = $messageId;
      $this->authCode = $authCode;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\MpsExpressCheckoutResponse
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return AuthCode
     */
    public function getAuthCode()
    {
      return $this->authCode;
    }

    /**
     * @param AuthCode $authCode
     * @return \GpWebpay\WsApi\MpsExpressCheckoutResponse
     */
    public function setAuthCode($authCode)
    {
      $this->authCode = $authCode;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\MpsExpressCheckoutResponse
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
