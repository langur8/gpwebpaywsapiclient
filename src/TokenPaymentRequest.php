<?php

namespace GpWebpay\WsApi;

class TokenPaymentRequest
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var Provider $provider
     */
    protected $provider = null;

    /**
     * @var MerchantNumber $merchantNumber
     */
    protected $merchantNumber = null;

    /**
     * @var PaymentNumber $paymentNumber
     */
    protected $paymentNumber = null;

    /**
     * @var OrderNumber $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var ReferenceNumber $referenceNumber
     */
    protected $referenceNumber = null;

    /**
     * @var Amount $amount
     */
    protected $amount = null;

    /**
     * @var CurrencyCode $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var CaptureFlag $captureFlag
     */
    protected $captureFlag = null;

    /**
     * @var SubMerchantData $subMerchantData
     */
    protected $subMerchantData = null;

    /**
     * @var TokenData $tokenData
     */
    protected $tokenData = null;

    /**
     * @var CardHolderData $cardHolderData
     */
    protected $cardHolderData = null;

    /**
     * @var AltTerminalData $altTerminalData
     */
    protected $altTerminalData = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param Provider $provider
     * @param MerchantNumber $merchantNumber
     * @param PaymentNumber $paymentNumber
     * @param Amount $amount
     * @param CurrencyCode $currencyCode
     * @param CaptureFlag $captureFlag
     * @param TokenData $tokenData
     * @param Signature $signature
     */
    public function __construct($messageId, $provider, $merchantNumber, $paymentNumber, $amount, $currencyCode, $captureFlag, $tokenData, $signature)
    {
      $this->messageId = $messageId;
      $this->provider = $provider;
      $this->merchantNumber = $merchantNumber;
      $this->paymentNumber = $paymentNumber;
      $this->amount = $amount;
      $this->currencyCode = $currencyCode;
      $this->captureFlag = $captureFlag;
      $this->tokenData = $tokenData;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
      return $this->provider;
    }

    /**
     * @param Provider $provider
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setProvider($provider)
    {
      $this->provider = $provider;
      return $this;
    }

    /**
     * @return MerchantNumber
     */
    public function getMerchantNumber()
    {
      return $this->merchantNumber;
    }

    /**
     * @param MerchantNumber $merchantNumber
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setMerchantNumber($merchantNumber)
    {
      $this->merchantNumber = $merchantNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPaymentNumber()
    {
      return $this->paymentNumber;
    }

    /**
     * @param PaymentNumber $paymentNumber
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setPaymentNumber($paymentNumber)
    {
      $this->paymentNumber = $paymentNumber;
      return $this;
    }

    /**
     * @return OrderNumber
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param OrderNumber $orderNumber
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return ReferenceNumber
     */
    public function getReferenceNumber()
    {
      return $this->referenceNumber;
    }

    /**
     * @param ReferenceNumber $referenceNumber
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setReferenceNumber($referenceNumber)
    {
      $this->referenceNumber = $referenceNumber;
      return $this;
    }

    /**
     * @return Amount
     */
    public function getAmount()
    {
      return $this->amount;
    }

    /**
     * @param Amount $amount
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setAmount($amount)
    {
      $this->amount = $amount;
      return $this;
    }

    /**
     * @return CurrencyCode
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param CurrencyCode $currencyCode
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return CaptureFlag
     */
    public function getCaptureFlag()
    {
      return $this->captureFlag;
    }

    /**
     * @param CaptureFlag $captureFlag
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setCaptureFlag($captureFlag)
    {
      $this->captureFlag = $captureFlag;
      return $this;
    }

    /**
     * @return SubMerchantData
     */
    public function getSubMerchantData()
    {
      return $this->subMerchantData;
    }

    /**
     * @param SubMerchantData $subMerchantData
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setSubMerchantData($subMerchantData)
    {
      $this->subMerchantData = $subMerchantData;
      return $this;
    }

    /**
     * @return TokenData
     */
    public function getTokenData()
    {
      return $this->tokenData;
    }

    /**
     * @param TokenData $tokenData
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setTokenData($tokenData)
    {
      $this->tokenData = $tokenData;
      return $this;
    }

    /**
     * @return CardHolderData
     */
    public function getCardHolderData()
    {
      return $this->cardHolderData;
    }

    /**
     * @param CardHolderData $cardHolderData
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setCardHolderData($cardHolderData)
    {
      $this->cardHolderData = $cardHolderData;
      return $this;
    }

    /**
     * @return AltTerminalData
     */
    public function getAltTerminalData()
    {
      return $this->altTerminalData;
    }

    /**
     * @param AltTerminalData $altTerminalData
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setAltTerminalData($altTerminalData)
    {
      $this->altTerminalData = $altTerminalData;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\TokenPaymentRequest
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
