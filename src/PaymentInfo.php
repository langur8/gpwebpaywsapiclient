<?php

namespace GpWebpay\WsApi;

class PaymentInfo
{

    /**
     * @var TransactionTypeValue $transactionType
     */
    protected $transactionType = null;

    /**
     * @var ShippingIndicatorValue $shippingIndicator
     */
    protected $shippingIndicator = null;

    /**
     * @var PreOrderPurchaseIndValue $preOrderPurchaseInd
     */
    protected $preOrderPurchaseInd = null;

    /**
     * @var DateTypeValue $preOrderDate
     */
    protected $preOrderDate = null;

    /**
     * @var ReorderItemsIndValue $reorderItemsInd
     */
    protected $reorderItemsInd = null;

    /**
     * @var DeliveryTimeframeValue $deliveryTimeframe
     */
    protected $deliveryTimeframe = null;

    /**
     * @var Email $deliveryEmailAddress
     */
    protected $deliveryEmailAddress = null;

    /**
     * @var GiftCardCountValue $giftCardCount
     */
    protected $giftCardCount = null;

    /**
     * @var GiftCardAmountValue $giftCardAmount
     */
    protected $giftCardAmount = null;

    /**
     * @var GiftCardCurrencyValue $giftCardCurrency
     */
    protected $giftCardCurrency = null;

    /**
     * @var DateTypeValue $recurringExpiry
     */
    protected $recurringExpiry = null;

    /**
     * @var RecurringFrequencyValue $recurringFrequency
     */
    protected $recurringFrequency = null;

    /**
     * @var RemmitanceInfoValue $remmitanceInfo1
     */
    protected $remmitanceInfo1 = null;

    /**
     * @var RemmitanceInfoValue $remmitanceInfo2
     */
    protected $remmitanceInfo2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TransactionTypeValue
     */
    public function getTransactionType()
    {
      return $this->transactionType;
    }

    /**
     * @param TransactionTypeValue $transactionType
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setTransactionType($transactionType)
    {
      $this->transactionType = $transactionType;
      return $this;
    }

    /**
     * @return ShippingIndicatorValue
     */
    public function getShippingIndicator()
    {
      return $this->shippingIndicator;
    }

    /**
     * @param ShippingIndicatorValue $shippingIndicator
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setShippingIndicator($shippingIndicator)
    {
      $this->shippingIndicator = $shippingIndicator;
      return $this;
    }

    /**
     * @return PreOrderPurchaseIndValue
     */
    public function getPreOrderPurchaseInd()
    {
      return $this->preOrderPurchaseInd;
    }

    /**
     * @param PreOrderPurchaseIndValue $preOrderPurchaseInd
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setPreOrderPurchaseInd($preOrderPurchaseInd)
    {
      $this->preOrderPurchaseInd = $preOrderPurchaseInd;
      return $this;
    }

    /**
     * @return DateTypeValue
     */
    public function getPreOrderDate()
    {
      return $this->preOrderDate;
    }

    /**
     * @param DateTypeValue $preOrderDate
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setPreOrderDate($preOrderDate)
    {
      $this->preOrderDate = $preOrderDate;
      return $this;
    }

    /**
     * @return ReorderItemsIndValue
     */
    public function getReorderItemsInd()
    {
      return $this->reorderItemsInd;
    }

    /**
     * @param ReorderItemsIndValue $reorderItemsInd
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setReorderItemsInd($reorderItemsInd)
    {
      $this->reorderItemsInd = $reorderItemsInd;
      return $this;
    }

    /**
     * @return DeliveryTimeframeValue
     */
    public function getDeliveryTimeframe()
    {
      return $this->deliveryTimeframe;
    }

    /**
     * @param DeliveryTimeframeValue $deliveryTimeframe
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setDeliveryTimeframe($deliveryTimeframe)
    {
      $this->deliveryTimeframe = $deliveryTimeframe;
      return $this;
    }

    /**
     * @return Email
     */
    public function getDeliveryEmailAddress()
    {
      return $this->deliveryEmailAddress;
    }

    /**
     * @param Email $deliveryEmailAddress
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setDeliveryEmailAddress($deliveryEmailAddress)
    {
      $this->deliveryEmailAddress = $deliveryEmailAddress;
      return $this;
    }

    /**
     * @return GiftCardCountValue
     */
    public function getGiftCardCount()
    {
      return $this->giftCardCount;
    }

    /**
     * @param GiftCardCountValue $giftCardCount
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setGiftCardCount($giftCardCount)
    {
      $this->giftCardCount = $giftCardCount;
      return $this;
    }

    /**
     * @return GiftCardAmountValue
     */
    public function getGiftCardAmount()
    {
      return $this->giftCardAmount;
    }

    /**
     * @param GiftCardAmountValue $giftCardAmount
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setGiftCardAmount($giftCardAmount)
    {
      $this->giftCardAmount = $giftCardAmount;
      return $this;
    }

    /**
     * @return GiftCardCurrencyValue
     */
    public function getGiftCardCurrency()
    {
      return $this->giftCardCurrency;
    }

    /**
     * @param GiftCardCurrencyValue $giftCardCurrency
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setGiftCardCurrency($giftCardCurrency)
    {
      $this->giftCardCurrency = $giftCardCurrency;
      return $this;
    }

    /**
     * @return DateTypeValue
     */
    public function getRecurringExpiry()
    {
      return $this->recurringExpiry;
    }

    /**
     * @param DateTypeValue $recurringExpiry
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setRecurringExpiry($recurringExpiry)
    {
      $this->recurringExpiry = $recurringExpiry;
      return $this;
    }

    /**
     * @return RecurringFrequencyValue
     */
    public function getRecurringFrequency()
    {
      return $this->recurringFrequency;
    }

    /**
     * @param RecurringFrequencyValue $recurringFrequency
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setRecurringFrequency($recurringFrequency)
    {
      $this->recurringFrequency = $recurringFrequency;
      return $this;
    }

    /**
     * @return RemmitanceInfoValue
     */
    public function getRemmitanceInfo1()
    {
      return $this->remmitanceInfo1;
    }

    /**
     * @param RemmitanceInfoValue $remmitanceInfo1
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setRemmitanceInfo1($remmitanceInfo1)
    {
      $this->remmitanceInfo1 = $remmitanceInfo1;
      return $this;
    }

    /**
     * @return RemmitanceInfoValue
     */
    public function getRemmitanceInfo2()
    {
      return $this->remmitanceInfo2;
    }

    /**
     * @param RemmitanceInfoValue $remmitanceInfo2
     * @return \GpWebpay\WsApi\PaymentInfo
     */
    public function setRemmitanceInfo2($remmitanceInfo2)
    {
      $this->remmitanceInfo2 = $remmitanceInfo2;
      return $this;
    }

}
