<?php

namespace GpWebpay\WsApi;

class MpsContact
{

    /**
     * @var string $firstName
     */
    protected $firstName = null;

    /**
     * @var middleName $middleName
     */
    protected $middleName = null;

    /**
     * @var string $lastName
     */
    protected $lastName = null;

    /**
     * @var string $country
     */
    protected $country = null;

    /**
     * @var string $emailAddress
     */
    protected $emailAddress = null;

    /**
     * @var string $phoneNumber
     */
    protected $phoneNumber = null;

    /**
     * @var SimpleValueHolder[] $simpleValueHolder
     */
    protected $simpleValueHolder = null;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $country
     * @param string $emailAddress
     * @param string $phoneNumber
     */
    public function __construct($firstName, $lastName, $country, $emailAddress, $phoneNumber)
    {
      $this->firstName = $firstName;
      $this->lastName = $lastName;
      $this->country = $country;
      $this->emailAddress = $emailAddress;
      $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
      return $this;
    }

    /**
     * @return middleName
     */
    public function getMiddleName()
    {
      return $this->middleName;
    }

    /**
     * @param middleName $middleName
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setMiddleName($middleName)
    {
      $this->middleName = $middleName;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param string $country
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
      return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setEmailAddress($emailAddress)
    {
      $this->emailAddress = $emailAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
      return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setPhoneNumber($phoneNumber)
    {
      $this->phoneNumber = $phoneNumber;
      return $this;
    }

    /**
     * @return SimpleValueHolder[]
     */
    public function getSimpleValueHolder()
    {
      return $this->simpleValueHolder;
    }

    /**
     * @param SimpleValueHolder[] $simpleValueHolder
     * @return \GpWebpay\WsApi\MpsContact
     */
    public function setSimpleValueHolder(array $simpleValueHolder = null)
    {
      $this->simpleValueHolder = $simpleValueHolder;
      return $this;
    }

}
