<?php

namespace GpWebpay\WsApi;

class processUsageBasedPaymentResponse
{

    /**
     * @var UsageBasedPaymentResponse $usageBasedPaymentResponse
     */
    protected $usageBasedPaymentResponse = null;

    /**
     * @param UsageBasedPaymentResponse $usageBasedPaymentResponse
     */
    public function __construct($usageBasedPaymentResponse)
    {
      $this->usageBasedPaymentResponse = $usageBasedPaymentResponse;
    }

    /**
     * @return UsageBasedPaymentResponse
     */
    public function getUsageBasedPaymentResponse()
    {
      return $this->usageBasedPaymentResponse;
    }

    /**
     * @param UsageBasedPaymentResponse $usageBasedPaymentResponse
     * @return \GpWebpay\WsApi\processUsageBasedPaymentResponse
     */
    public function setUsageBasedPaymentResponse($usageBasedPaymentResponse)
    {
      $this->usageBasedPaymentResponse = $usageBasedPaymentResponse;
      return $this;
    }

}
