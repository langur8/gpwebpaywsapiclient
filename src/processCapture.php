<?php

namespace GpWebpay\WsApi;

class processCapture
{

    /**
     * @var CaptureRequest $captureRequest
     */
    protected $captureRequest = null;

    /**
     * @param CaptureRequest $captureRequest
     */
    public function __construct($captureRequest)
    {
      $this->captureRequest = $captureRequest;
    }

    /**
     * @return CaptureRequest
     */
    public function getCaptureRequest()
    {
      return $this->captureRequest;
    }

    /**
     * @param CaptureRequest $captureRequest
     * @return \GpWebpay\WsApi\processCapture
     */
    public function setCaptureRequest($captureRequest)
    {
      $this->captureRequest = $captureRequest;
      return $this;
    }

}
