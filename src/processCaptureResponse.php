<?php

namespace GpWebpay\WsApi;

class processCaptureResponse
{

    /**
     * @var CaptureResponse $captureResponse
     */
    protected $captureResponse = null;

    /**
     * @param CaptureResponse $captureResponse
     */
    public function __construct($captureResponse)
    {
      $this->captureResponse = $captureResponse;
    }

    /**
     * @return CaptureResponse
     */
    public function getCaptureResponse()
    {
      return $this->captureResponse;
    }

    /**
     * @param CaptureResponse $captureResponse
     * @return \GpWebpay\WsApi\processCaptureResponse
     */
    public function setCaptureResponse($captureResponse)
    {
      $this->captureResponse = $captureResponse;
      return $this;
    }

}
