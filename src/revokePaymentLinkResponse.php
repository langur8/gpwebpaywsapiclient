<?php

namespace GpWebpay\WsApi;

class revokePaymentLinkResponse
{

    /**
     * @var RevokePaymentLinkResponse $revokePaymentLinkResponse
     */
    protected $revokePaymentLinkResponse = null;

    /**
     * @param RevokePaymentLinkResponse $revokePaymentLinkResponse
     */
    public function __construct($revokePaymentLinkResponse)
    {
      $this->revokePaymentLinkResponse = $revokePaymentLinkResponse;
    }

    /**
     * @return RevokePaymentLinkResponse
     */
    public function getRevokePaymentLinkResponse()
    {
      return $this->revokePaymentLinkResponse;
    }

    /**
     * @param RevokePaymentLinkResponse $revokePaymentLinkResponse
     * @return \GpWebpay\WsApi\revokePaymentLinkResponse
     */
    public function setRevokePaymentLinkResponse($revokePaymentLinkResponse)
    {
      $this->revokePaymentLinkResponse = $revokePaymentLinkResponse;
      return $this;
    }

}
