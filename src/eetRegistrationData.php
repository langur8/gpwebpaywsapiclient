<?php

namespace GpWebpay\WsApi;

class eetRegistrationData
{

    /**
     * @var czTaxIdValue $taxId
     */
    protected $taxId = null;

    /**
     * @var businessPremisesIdValue $businessPremisesId
     */
    protected $businessPremisesId = null;

    /**
     * @var cashRegisterIdValue $cashRegisterId
     */
    protected $cashRegisterId = null;

    /**
     * @var receiptNumberValue $receiptNumber
     */
    protected $receiptNumber = null;

    /**
     * @var textValue $saleDateTime
     */
    protected $saleDateTime = null;

    /**
     * @var amountValue $totalSaleAmount
     */
    protected $totalSaleAmount = null;

    /**
     * @var regimeValue $regime
     */
    protected $regime = null;

    /**
     * @var fiscalCodeValue $fiscalCode
     */
    protected $fiscalCode = null;

    /**
     * @var securityCodeValue $securityCode
     */
    protected $securityCode = null;

    /**
     * @var signatureCodeValue $signatureCode
     */
    protected $signatureCode = null;

    /**
     * @param czTaxIdValue $taxId
     * @param businessPremisesIdValue $businessPremisesId
     * @param cashRegisterIdValue $cashRegisterId
     * @param receiptNumberValue $receiptNumber
     * @param textValue $saleDateTime
     * @param amountValue $totalSaleAmount
     * @param regimeValue $regime
     * @param fiscalCodeValue $fiscalCode
     * @param securityCodeValue $securityCode
     * @param signatureCodeValue $signatureCode
     */
    public function __construct($taxId, $businessPremisesId, $cashRegisterId, $receiptNumber, $saleDateTime, $totalSaleAmount, $regime, $fiscalCode, $securityCode, $signatureCode)
    {
      $this->taxId = $taxId;
      $this->businessPremisesId = $businessPremisesId;
      $this->cashRegisterId = $cashRegisterId;
      $this->receiptNumber = $receiptNumber;
      $this->saleDateTime = $saleDateTime;
      $this->totalSaleAmount = $totalSaleAmount;
      $this->regime = $regime;
      $this->fiscalCode = $fiscalCode;
      $this->securityCode = $securityCode;
      $this->signatureCode = $signatureCode;
    }

    /**
     * @return czTaxIdValue
     */
    public function getTaxId()
    {
      return $this->taxId;
    }

    /**
     * @param czTaxIdValue $taxId
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setTaxId($taxId)
    {
      $this->taxId = $taxId;
      return $this;
    }

    /**
     * @return businessPremisesIdValue
     */
    public function getBusinessPremisesId()
    {
      return $this->businessPremisesId;
    }

    /**
     * @param businessPremisesIdValue $businessPremisesId
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setBusinessPremisesId($businessPremisesId)
    {
      $this->businessPremisesId = $businessPremisesId;
      return $this;
    }

    /**
     * @return cashRegisterIdValue
     */
    public function getCashRegisterId()
    {
      return $this->cashRegisterId;
    }

    /**
     * @param cashRegisterIdValue $cashRegisterId
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setCashRegisterId($cashRegisterId)
    {
      $this->cashRegisterId = $cashRegisterId;
      return $this;
    }

    /**
     * @return receiptNumberValue
     */
    public function getReceiptNumber()
    {
      return $this->receiptNumber;
    }

    /**
     * @param receiptNumberValue $receiptNumber
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setReceiptNumber($receiptNumber)
    {
      $this->receiptNumber = $receiptNumber;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getSaleDateTime()
    {
      return $this->saleDateTime;
    }

    /**
     * @param textValue $saleDateTime
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setSaleDateTime($saleDateTime)
    {
      $this->saleDateTime = $saleDateTime;
      return $this;
    }

    /**
     * @return amountValue
     */
    public function getTotalSaleAmount()
    {
      return $this->totalSaleAmount;
    }

    /**
     * @param amountValue $totalSaleAmount
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setTotalSaleAmount($totalSaleAmount)
    {
      $this->totalSaleAmount = $totalSaleAmount;
      return $this;
    }

    /**
     * @return regimeValue
     */
    public function getRegime()
    {
      return $this->regime;
    }

    /**
     * @param regimeValue $regime
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setRegime($regime)
    {
      $this->regime = $regime;
      return $this;
    }

    /**
     * @return fiscalCodeValue
     */
    public function getFiscalCode()
    {
      return $this->fiscalCode;
    }

    /**
     * @param fiscalCodeValue $fiscalCode
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setFiscalCode($fiscalCode)
    {
      $this->fiscalCode = $fiscalCode;
      return $this;
    }

    /**
     * @return securityCodeValue
     */
    public function getSecurityCode()
    {
      return $this->securityCode;
    }

    /**
     * @param securityCodeValue $securityCode
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setSecurityCode($securityCode)
    {
      $this->securityCode = $securityCode;
      return $this;
    }

    /**
     * @return signatureCodeValue
     */
    public function getSignatureCode()
    {
      return $this->signatureCode;
    }

    /**
     * @param signatureCodeValue $signatureCode
     * @return \GpWebpay\WsApi\eetRegistrationData
     */
    public function setSignatureCode($signatureCode)
    {
      $this->signatureCode = $signatureCode;
      return $this;
    }

}
