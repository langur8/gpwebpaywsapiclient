<?php

namespace GpWebpay\WsApi;

class MpsExpressCheckoutRequest
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var Provider $provider
     */
    protected $provider = null;

    /**
     * @var MerchantNumber $merchantNumber
     */
    protected $merchantNumber = null;

    /**
     * @var PaymentNumber $paymentNumber
     */
    protected $paymentNumber = null;

    /**
     * @var OrderNumber $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var ReferenceNumber $referenceNumber
     */
    protected $referenceNumber = null;

    /**
     * @var Amount $amount
     */
    protected $amount = null;

    /**
     * @var CurrencyCode $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var CaptureFlag $captureFlag
     */
    protected $captureFlag = null;

    /**
     * @var PaymentNumber $pairingNumber
     */
    protected $pairingNumber = null;

    /**
     * @var TextValue $cardId
     */
    protected $cardId = null;

    /**
     * @var TextValue $shippingAddressId
     */
    protected $shippingAddressId = null;

    /**
     * @var CardHolderData $cardHolderData
     */
    protected $cardHolderData = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param Provider $provider
     * @param MerchantNumber $merchantNumber
     * @param PaymentNumber $paymentNumber
     * @param Amount $amount
     * @param CurrencyCode $currencyCode
     * @param CaptureFlag $captureFlag
     * @param PaymentNumber $pairingNumber
     * @param TextValue $cardId
     * @param Signature $signature
     */
    public function __construct($messageId, $provider, $merchantNumber, $paymentNumber, $amount, $currencyCode, $captureFlag, $pairingNumber, $cardId, $signature)
    {
      $this->messageId = $messageId;
      $this->provider = $provider;
      $this->merchantNumber = $merchantNumber;
      $this->paymentNumber = $paymentNumber;
      $this->amount = $amount;
      $this->currencyCode = $currencyCode;
      $this->captureFlag = $captureFlag;
      $this->pairingNumber = $pairingNumber;
      $this->cardId = $cardId;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
      return $this->provider;
    }

    /**
     * @param Provider $provider
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setProvider($provider)
    {
      $this->provider = $provider;
      return $this;
    }

    /**
     * @return MerchantNumber
     */
    public function getMerchantNumber()
    {
      return $this->merchantNumber;
    }

    /**
     * @param MerchantNumber $merchantNumber
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setMerchantNumber($merchantNumber)
    {
      $this->merchantNumber = $merchantNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPaymentNumber()
    {
      return $this->paymentNumber;
    }

    /**
     * @param PaymentNumber $paymentNumber
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setPaymentNumber($paymentNumber)
    {
      $this->paymentNumber = $paymentNumber;
      return $this;
    }

    /**
     * @return OrderNumber
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param OrderNumber $orderNumber
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return ReferenceNumber
     */
    public function getReferenceNumber()
    {
      return $this->referenceNumber;
    }

    /**
     * @param ReferenceNumber $referenceNumber
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setReferenceNumber($referenceNumber)
    {
      $this->referenceNumber = $referenceNumber;
      return $this;
    }

    /**
     * @return Amount
     */
    public function getAmount()
    {
      return $this->amount;
    }

    /**
     * @param Amount $amount
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setAmount($amount)
    {
      $this->amount = $amount;
      return $this;
    }

    /**
     * @return CurrencyCode
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param CurrencyCode $currencyCode
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return CaptureFlag
     */
    public function getCaptureFlag()
    {
      return $this->captureFlag;
    }

    /**
     * @param CaptureFlag $captureFlag
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setCaptureFlag($captureFlag)
    {
      $this->captureFlag = $captureFlag;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPairingNumber()
    {
      return $this->pairingNumber;
    }

    /**
     * @param PaymentNumber $pairingNumber
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setPairingNumber($pairingNumber)
    {
      $this->pairingNumber = $pairingNumber;
      return $this;
    }

    /**
     * @return TextValue
     */
    public function getCardId()
    {
      return $this->cardId;
    }

    /**
     * @param TextValue $cardId
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setCardId($cardId)
    {
      $this->cardId = $cardId;
      return $this;
    }

    /**
     * @return TextValue
     */
    public function getShippingAddressId()
    {
      return $this->shippingAddressId;
    }

    /**
     * @param TextValue $shippingAddressId
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setShippingAddressId($shippingAddressId)
    {
      $this->shippingAddressId = $shippingAddressId;
      return $this;
    }

    /**
     * @return CardHolderData
     */
    public function getCardHolderData()
    {
      return $this->cardHolderData;
    }

    /**
     * @param CardHolderData $cardHolderData
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setCardHolderData($cardHolderData)
    {
      $this->cardHolderData = $cardHolderData;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\MpsExpressCheckoutRequest
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
