<?php

namespace GpWebpay\WsApi;

class MpsPreCheckoutShippingAddress extends MpsAddress
{

    /**
     * @var string $recipientName
     */
    protected $recipientName = null;

    /**
     * @var string $recipientPhoneNumber
     */
    protected $recipientPhoneNumber = null;

    /**
     * @var string $addressId
     */
    protected $addressId = null;

    /**
     * @var boolean $selectedAsDefault
     */
    protected $selectedAsDefault = null;

    /**
     * @var string $shippingAlias
     */
    protected $shippingAlias = null;

    /**
     * @param string $city
     * @param string $country
     * @param string $address1
     * @param string $recipientName
     * @param string $recipientPhoneNumber
     * @param string $addressId
     * @param boolean $selectedAsDefault
     */
    public function __construct($city, $country, $address1, $recipientName, $recipientPhoneNumber, $addressId, $selectedAsDefault)
    {
      parent::__construct($city, $country, $address1);
      $this->recipientName = $recipientName;
      $this->recipientPhoneNumber = $recipientPhoneNumber;
      $this->addressId = $addressId;
      $this->selectedAsDefault = $selectedAsDefault;
    }

    /**
     * @return string
     */
    public function getRecipientName()
    {
      return $this->recipientName;
    }

    /**
     * @param string $recipientName
     * @return \GpWebpay\WsApi\MpsPreCheckoutShippingAddress
     */
    public function setRecipientName($recipientName)
    {
      $this->recipientName = $recipientName;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecipientPhoneNumber()
    {
      return $this->recipientPhoneNumber;
    }

    /**
     * @param string $recipientPhoneNumber
     * @return \GpWebpay\WsApi\MpsPreCheckoutShippingAddress
     */
    public function setRecipientPhoneNumber($recipientPhoneNumber)
    {
      $this->recipientPhoneNumber = $recipientPhoneNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddressId()
    {
      return $this->addressId;
    }

    /**
     * @param string $addressId
     * @return \GpWebpay\WsApi\MpsPreCheckoutShippingAddress
     */
    public function setAddressId($addressId)
    {
      $this->addressId = $addressId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSelectedAsDefault()
    {
      return $this->selectedAsDefault;
    }

    /**
     * @param boolean $selectedAsDefault
     * @return \GpWebpay\WsApi\MpsPreCheckoutShippingAddress
     */
    public function setSelectedAsDefault($selectedAsDefault)
    {
      $this->selectedAsDefault = $selectedAsDefault;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingAlias()
    {
      return $this->shippingAlias;
    }

    /**
     * @param string $shippingAlias
     * @return \GpWebpay\WsApi\MpsPreCheckoutShippingAddress
     */
    public function setShippingAlias($shippingAlias)
    {
      $this->shippingAlias = $shippingAlias;
      return $this;
    }

}
