<?php

namespace GpWebpay\WsApi;

use dlouhy\Soap\Client;

class PaymentService extends Client
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'additionalInfoResponse' => 'GpWebpay\\WsApi\\additionalInfoResponse',
      'contact' => 'GpWebpay\\WsApi\\contact',
      'billingDetails' => 'GpWebpay\\WsApi\\billingDetails',
      'shippingDetails' => 'GpWebpay\\WsApi\\shippingDetails',
      'cardsDetails' => 'GpWebpay\\WsApi\\cardsDetails',
      'cardDetail' => 'GpWebpay\\WsApi\\cardDetail',
      'loyaltyProgramDetails' => 'GpWebpay\\WsApi\\loyaltyProgramDetails',
      'eetRegistrationData' => 'GpWebpay\\WsApi\\eetRegistrationData',
      'EchoFaultDetail' => 'GpWebpay\\WsApi\\EchoFaultDetail',
      'FaultDetail' => 'GpWebpay\\WsApi\\FaultDetail',
      'PaymentFaultDetail' => 'GpWebpay\\WsApi\\PaymentFaultDetail',
      'CardOnFilePaymentFaultDetail' => 'GpWebpay\\WsApi\\CardOnFilePaymentFaultDetail',
      'PaymentStatusRequest' => 'GpWebpay\\WsApi\\PaymentStatusRequest',
      'PaymentStatusResponse' => 'GpWebpay\\WsApi\\PaymentStatusResponse',
      'PaymentLinkStatusRequest' => 'GpWebpay\\WsApi\\PaymentLinkStatusRequest',
      'PaymentLinkStatusResponse' => 'GpWebpay\\WsApi\\PaymentLinkStatusResponse',
      'MasterPaymentStatusResponse' => 'GpWebpay\\WsApi\\MasterPaymentStatusResponse',
      'RevokePaymentLinkResponse' => 'GpWebpay\\WsApi\\RevokePaymentLinkResponse',
      'SimpleValueHolder' => 'GpWebpay\\WsApi\\SimpleValueHolder',
      'PaymentDetailResponse' => 'GpWebpay\\WsApi\\PaymentDetailResponse',
      'AuthorizationReverseRequest' => 'GpWebpay\\WsApi\\AuthorizationReverseRequest',
      'AuthorizationReverseResponse' => 'GpWebpay\\WsApi\\AuthorizationReverseResponse',
      'BatchCloseRequest' => 'GpWebpay\\WsApi\\BatchCloseRequest',
      'BatchCloseResponse' => 'GpWebpay\\WsApi\\BatchCloseResponse',
      'RefundRequest' => 'GpWebpay\\WsApi\\RefundRequest',
      'RefundResponse' => 'GpWebpay\\WsApi\\RefundResponse',
      'RefundReverseRequest' => 'GpWebpay\\WsApi\\RefundReverseRequest',
      'RefundReverseResponse' => 'GpWebpay\\WsApi\\RefundReverseResponse',
      'PaymentDeleteRequest' => 'GpWebpay\\WsApi\\PaymentDeleteRequest',
      'PaymentDeleteResponse' => 'GpWebpay\\WsApi\\PaymentDeleteResponse',
      'CaptureRequest' => 'GpWebpay\\WsApi\\CaptureRequest',
      'CaptureResponse' => 'GpWebpay\\WsApi\\CaptureResponse',
      'CaptureReverseRequest' => 'GpWebpay\\WsApi\\CaptureReverseRequest',
      'CaptureReverseResponse' => 'GpWebpay\\WsApi\\CaptureReverseResponse',
      'PaymentCloseRequest' => 'GpWebpay\\WsApi\\PaymentCloseRequest',
      'PaymentCloseResponse' => 'GpWebpay\\WsApi\\PaymentCloseResponse',
      'RecurringPaymentRequest' => 'GpWebpay\\WsApi\\RecurringPaymentRequest',
      'RecurringPaymentResponse' => 'GpWebpay\\WsApi\\RecurringPaymentResponse',
      'PaymentLinkRequest' => 'GpWebpay\\WsApi\\PaymentLinkRequest',
      'PaymentLinkResponse' => 'GpWebpay\\WsApi\\PaymentLinkResponse',
      'MpsAddress' => 'GpWebpay\\WsApi\\MpsAddress',
      'MpsPreCheckoutShippingAddress' => 'GpWebpay\\WsApi\\MpsPreCheckoutShippingAddress',
      'MpsContact' => 'GpWebpay\\WsApi\\MpsContact',
      'MpsPreCheckoutCard' => 'GpWebpay\\WsApi\\MpsPreCheckoutCard',
      'MpsPreCheckoutRewardProgram' => 'GpWebpay\\WsApi\\MpsPreCheckoutRewardProgram',
      'MpsPreCheckoutData' => 'GpWebpay\\WsApi\\MpsPreCheckoutData',
      'MpsPreCheckoutRequest' => 'GpWebpay\\WsApi\\MpsPreCheckoutRequest',
      'MpsPreCheckoutResponse' => 'GpWebpay\\WsApi\\MpsPreCheckoutResponse',
      'MpsExpressCheckoutRequest' => 'GpWebpay\\WsApi\\MpsExpressCheckoutRequest',
      'MpsExpressCheckoutResponse' => 'GpWebpay\\WsApi\\MpsExpressCheckoutResponse',
      'CardHolderData' => 'GpWebpay\\WsApi\\CardHolderData',
      'cardholderDetails' => 'GpWebpay\\WsApi\\cardholderDetails',
      'ResolvePaymentStatusRequest' => 'GpWebpay\\WsApi\\ResolvePaymentStatusRequest',
      'TokenStatusRequest' => 'GpWebpay\\WsApi\\TokenStatusRequest',
      'TokenStatusResponse' => 'GpWebpay\\WsApi\\TokenStatusResponse',
      'TokenRevokeRequest' => 'GpWebpay\\WsApi\\TokenRevokeRequest',
      'TokenRevokeResponse' => 'GpWebpay\\WsApi\\TokenRevokeResponse',
      'TokenPaymentRequest' => 'GpWebpay\\WsApi\\TokenPaymentRequest',
      'TokenPaymentResponse' => 'GpWebpay\\WsApi\\TokenPaymentResponse',
      'AltTerminalData' => 'GpWebpay\\WsApi\\AltTerminalData',
      'SubMerchantData' => 'GpWebpay\\WsApi\\SubMerchantData',
      'CardOnFilePaymentRequest' => 'GpWebpay\\WsApi\\CardOnFilePaymentRequest',
      'UsageBasedSubscriptionPaymentRequest' => 'GpWebpay\\WsApi\\UsageBasedSubscriptionPaymentRequest',
      'RegularSubscriptionPaymentRequest' => 'GpWebpay\\WsApi\\RegularSubscriptionPaymentRequest',
      'PrepaidPaymentRequest' => 'GpWebpay\\WsApi\\PrepaidPaymentRequest',
      'UsageBasedPaymentResponse' => 'GpWebpay\\WsApi\\UsageBasedPaymentResponse',
      'UsageBasedSubscriptionPaymentResponse' => 'GpWebpay\\WsApi\\UsageBasedSubscriptionPaymentResponse',
      'RegularSubscriptionPaymentResponse' => 'GpWebpay\\WsApi\\RegularSubscriptionPaymentResponse',
      'PrepaidPaymentResponse' => 'GpWebpay\\WsApi\\PrepaidPaymentResponse',
      'CardOnFilePaymentResponse' => 'GpWebpay\\WsApi\\CardOnFilePaymentResponse',
      'PaymentInfo' => 'GpWebpay\\WsApi\\PaymentInfo',
      'ShoppingCartItem' => 'GpWebpay\\WsApi\\ShoppingCartItem',
      'ShoppingCartInfo' => 'GpWebpay\\WsApi\\ShoppingCartInfo',
      'shoppingCartItems' => 'GpWebpay\\WsApi\\shoppingCartItems',
      'echo' => 'GpWebpay\\WsApi\\echoCustom',
      'echoResponse' => 'GpWebpay\\WsApi\\echoResponse',
      'getPaymentStatus' => 'GpWebpay\\WsApi\\getPaymentStatus',
      'getPaymentStatusResponse' => 'GpWebpay\\WsApi\\getPaymentStatusResponse',
      'getPaymentLinkStatus' => 'GpWebpay\\WsApi\\getPaymentLinkStatus',
      'getPaymentLinkStatusResponse' => 'GpWebpay\\WsApi\\getPaymentLinkStatusResponse',
      'getMasterPaymentStatus' => 'GpWebpay\\WsApi\\getMasterPaymentStatus',
      'getMasterPaymentStatusResponse' => 'GpWebpay\\WsApi\\getMasterPaymentStatusResponse',
      'processMasterPaymentRevoke' => 'GpWebpay\\WsApi\\processMasterPaymentRevoke',
      'processMasterPaymentRevokeResponse' => 'GpWebpay\\WsApi\\processMasterPaymentRevokeResponse',
      'revokePaymentLink' => 'GpWebpay\\WsApi\\revokePaymentLink',
      'revokePaymentLinkResponse' => 'GpWebpay\\WsApi\\revokePaymentLinkResponse',
      'getPaymentDetail' => 'GpWebpay\\WsApi\\getPaymentDetail',
      'getPaymentDetailResponse' => 'GpWebpay\\WsApi\\getPaymentDetailResponse',
      'processAuthorizationReverse' => 'GpWebpay\\WsApi\\processAuthorizationReverse',
      'processAuthorizationReverseResponse' => 'GpWebpay\\WsApi\\processAuthorizationReverseResponse',
      'processBatchClose' => 'GpWebpay\\WsApi\\processBatchClose',
      'processBatchCloseResponse' => 'GpWebpay\\WsApi\\processBatchCloseResponse',
      'processRefund' => 'GpWebpay\\WsApi\\processRefund',
      'processRefundResponse' => 'GpWebpay\\WsApi\\processRefundResponse',
      'processRefundReverse' => 'GpWebpay\\WsApi\\processRefundReverse',
      'processRefundReverseResponse' => 'GpWebpay\\WsApi\\processRefundReverseResponse',
      'processPaymentDelete' => 'GpWebpay\\WsApi\\processPaymentDelete',
      'processPaymentDeleteResponse' => 'GpWebpay\\WsApi\\processPaymentDeleteResponse',
      'processCapture' => 'GpWebpay\\WsApi\\processCapture',
      'processCaptureResponse' => 'GpWebpay\\WsApi\\processCaptureResponse',
      'processCaptureReverse' => 'GpWebpay\\WsApi\\processCaptureReverse',
      'processCaptureReverseResponse' => 'GpWebpay\\WsApi\\processCaptureReverseResponse',
      'processPaymentClose' => 'GpWebpay\\WsApi\\processPaymentClose',
      'processPaymentCloseResponse' => 'GpWebpay\\WsApi\\processPaymentCloseResponse',
      'processRecurringPayment' => 'GpWebpay\\WsApi\\processRecurringPayment',
      'processRecurringPaymentResponse' => 'GpWebpay\\WsApi\\processRecurringPaymentResponse',
      'processUsageBasedPayment' => 'GpWebpay\\WsApi\\processUsageBasedPayment',
      'processUsageBasedPaymentResponse' => 'GpWebpay\\WsApi\\processUsageBasedPaymentResponse',
      'processRegularSubscriptionPayment' => 'GpWebpay\\WsApi\\processRegularSubscriptionPayment',
      'processRegularSubscriptionPaymentResponse' => 'GpWebpay\\WsApi\\processRegularSubscriptionPaymentResponse',
      'processUsageBasedSubscriptionPayment' => 'GpWebpay\\WsApi\\processUsageBasedSubscriptionPayment',
      'processUsageBasedSubscriptionPaymentResponse' => 'GpWebpay\\WsApi\\processUsageBasedSubscriptionPaymentResponse',
      'processPrepaidPayment' => 'GpWebpay\\WsApi\\processPrepaidPayment',
      'processPrepaidPaymentResponse' => 'GpWebpay\\WsApi\\processPrepaidPaymentResponse',
      'processCardOnFilePayment' => 'GpWebpay\\WsApi\\processCardOnFilePayment',
      'processCardOnFilePaymentResponse' => 'GpWebpay\\WsApi\\processCardOnFilePaymentResponse',
      'createPaymentLink' => 'GpWebpay\\WsApi\\createPaymentLink',
      'createPaymentLinkResponse' => 'GpWebpay\\WsApi\\createPaymentLinkResponse',
      'mpsPreCheckout' => 'GpWebpay\\WsApi\\mpsPreCheckout',
      'mpsPreCheckoutResponse' => 'GpWebpay\\WsApi\\mpsPreCheckoutResponse',
      'mpsExpressCheckout' => 'GpWebpay\\WsApi\\mpsExpressCheckout',
      'mpsExpressCheckoutResponse' => 'GpWebpay\\WsApi\\mpsExpressCheckoutResponse',
      'resolvePaymentStatus' => 'GpWebpay\\WsApi\\resolvePaymentStatus',
      'resolvePaymentStatusResponse' => 'GpWebpay\\WsApi\\resolvePaymentStatusResponse',
      'getTokenStatus' => 'GpWebpay\\WsApi\\getTokenStatus',
      'getTokenStatusResponse' => 'GpWebpay\\WsApi\\getTokenStatusResponse',
      'processTokenRevoke' => 'GpWebpay\\WsApi\\processTokenRevoke',
      'processTokenRevokeResponse' => 'GpWebpay\\WsApi\\processTokenRevokeResponse',
      'processTokenPayment' => 'GpWebpay\\WsApi\\processTokenPayment',
      'processTokenPaymentResponse' => 'GpWebpay\\WsApi\\processTokenPaymentResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = './cws_v1.wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param echoCustom $parameters
     * @return echoResponse
     */
    public function aEcho($parameters)
    {
      return $this->__soapCall('echo', array($parameters));
    }

    /**
     * @param getPaymentStatus $parameters
     * @return getPaymentStatusResponse
     */
    public function getPaymentStatus(getPaymentStatus $parameters)
    {
      return $this->__soapCall('getPaymentStatus', array($parameters));
    }

    /**
     * @param getPaymentLinkStatus $parameters
     * @return getPaymentLinkStatusResponse
     */
    public function getPaymentLinkStatus(getPaymentLinkStatus $parameters)
    {
      return $this->__soapCall('getPaymentLinkStatus', array($parameters));
    }

    /**
     * @param getMasterPaymentStatus $parameters
     * @return getMasterPaymentStatusResponse
     */
    public function getMasterPaymentStatus(getMasterPaymentStatus $parameters)
    {
      return $this->__soapCall('getMasterPaymentStatus', array($parameters));
    }

    /**
     * @param processMasterPaymentRevoke $parameters
     * @return processMasterPaymentRevokeResponse
     */
    public function processMasterPaymentRevoke(processMasterPaymentRevoke $parameters)
    {
      return $this->__soapCall('processMasterPaymentRevoke', array($parameters));
    }

    /**
     * @param revokePaymentLink $parameters
     * @return revokePaymentLinkResponse
     */
    public function revokePaymentLink(revokePaymentLink $parameters)
    {
      return $this->__soapCall('revokePaymentLink', array($parameters));
    }

    /**
     * @param getPaymentDetail $parameters
     * @return getPaymentDetailResponse
     */
    public function getPaymentDetail(getPaymentDetail $parameters)
    {
      return $this->__soapCall('getPaymentDetail', array($parameters));
    }

    /**
     * @param processAuthorizationReverse $parameters
     * @return processAuthorizationReverseResponse
     */
    public function processAuthorizationReverse(processAuthorizationReverse $parameters)
    {
      return $this->__soapCall('processAuthorizationReverse', array($parameters));
    }

    /**
     * @param processBatchClose $parameters
     * @return processBatchCloseResponse
     */
    public function processBatchClose(processBatchClose $parameters)
    {
      return $this->__soapCall('processBatchClose', array($parameters));
    }

    /**
     * @param processRefund $parameters
     * @return processRefundResponse
     */
    public function processRefund(processRefund $parameters)
    {
      return $this->__soapCall('processRefund', array($parameters));
    }

    /**
     * @param processRefundReverse $parameters
     * @return processRefundReverseResponse
     */
    public function processRefundReverse(processRefundReverse $parameters)
    {
      return $this->__soapCall('processRefundReverse', array($parameters));
    }

    /**
     * @param processPaymentDelete $parameters
     * @return processPaymentDeleteResponse
     */
    public function processPaymentDelete(processPaymentDelete $parameters)
    {
      return $this->__soapCall('processPaymentDelete', array($parameters));
    }

    /**
     * @param processCapture $parameters
     * @return processCaptureResponse
     */
    public function processCapture(processCapture $parameters)
    {
      return $this->__soapCall('processCapture', array($parameters));
    }

    /**
     * @param processCaptureReverse $parameters
     * @return processCaptureReverseResponse
     */
    public function processCaptureReverse(processCaptureReverse $parameters)
    {
      return $this->__soapCall('processCaptureReverse', array($parameters));
    }

    /**
     * @param processPaymentClose $parameters
     * @return processPaymentCloseResponse
     */
    public function processPaymentClose(processPaymentClose $parameters)
    {
      return $this->__soapCall('processPaymentClose', array($parameters));
    }

    /**
     * @param processRecurringPayment $parameters
     * @return processRecurringPaymentResponse
     */
    public function processRecurringPayment(processRecurringPayment $parameters)
    {
      return $this->__soapCall('processRecurringPayment', array($parameters));
    }

    /**
     * @param processUsageBasedPayment $parameters
     * @return processUsageBasedPaymentResponse
     */
    public function processUsageBasedPayment(processUsageBasedPayment $parameters)
    {
      return $this->__soapCall('processUsageBasedPayment', array($parameters));
    }

    /**
     * @param processUsageBasedSubscriptionPayment $parameters
     * @return processUsageBasedSubscriptionPaymentResponse
     */
    public function processUsageBasedSubscriptionPayment(processUsageBasedSubscriptionPayment $parameters)
    {
      return $this->__soapCall('processUsageBasedSubscriptionPayment', array($parameters));
    }

    /**
     * @param processRegularSubscriptionPayment $parameters
     * @return processRegularSubscriptionPaymentResponse
     */
    public function processRegularSubscriptionPayment(processRegularSubscriptionPayment $parameters)
    {
      return $this->__soapCall('processRegularSubscriptionPayment', array($parameters));
    }

    /**
     * @param processPrepaidPayment $parameters
     * @return processPrepaidPaymentResponse
     */
    public function processPrepaidPayment(processPrepaidPayment $parameters)
    {
      return $this->__soapCall('processPrepaidPayment', array($parameters));
    }

    /**
     * @param processCardOnFilePayment $parameters
     * @return processCardOnFilePaymentResponse
     */
    public function processCardOnFilePayment(processCardOnFilePayment $parameters)
    {
      return $this->__soapCall('processCardOnFilePayment', array($parameters));
    }

    /**
     * @param createPaymentLink $parameters
     * @return createPaymentLinkResponse
     */
    public function createPaymentLink(createPaymentLink $parameters)
    {
      return $this->__soapCall('createPaymentLink', array($parameters));
    }

    /**
     * @param mpsPreCheckout $parameters
     * @return mpsPreCheckoutResponse
     */
    public function mpsPreCheckout(mpsPreCheckout $parameters)
    {
      return $this->__soapCall('mpsPreCheckout', array($parameters));
    }

    /**
     * @param mpsExpressCheckout $parameters
     * @return mpsExpressCheckoutResponse
     */
    public function mpsExpressCheckout(mpsExpressCheckout $parameters)
    {
      return $this->__soapCall('mpsExpressCheckout', array($parameters));
    }

    /**
     * @param resolvePaymentStatus $parameters
     * @return resolvePaymentStatusResponse
     */
    public function resolvePaymentStatus(resolvePaymentStatus $parameters)
    {
      return $this->__soapCall('resolvePaymentStatus', array($parameters));
    }

    /**
     * @param getTokenStatus $parameters
     * @return getTokenStatusResponse
     */
    public function getTokenStatus(getTokenStatus $parameters)
    {
      return $this->__soapCall('getTokenStatus', array($parameters));
    }

    /**
     * @param processTokenRevoke $parameters
     * @return processTokenRevokeResponse
     */
    public function processTokenRevoke(processTokenRevoke $parameters)
    {
      return $this->__soapCall('processTokenRevoke', array($parameters));
    }

    /**
     * @param processTokenPayment $parameters
     * @return processTokenPaymentResponse
     */
    public function processTokenPayment(processTokenPayment $parameters)
    {
      return $this->__soapCall('processTokenPayment', array($parameters));
    }

}
