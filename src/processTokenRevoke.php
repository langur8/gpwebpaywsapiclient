<?php

namespace GpWebpay\WsApi;

class processTokenRevoke
{

    /**
     * @var TokenRevokeRequest $tokenRevokeRequest
     */
    protected $tokenRevokeRequest = null;

    /**
     * @param TokenRevokeRequest $tokenRevokeRequest
     */
    public function __construct($tokenRevokeRequest)
    {
      $this->tokenRevokeRequest = $tokenRevokeRequest;
    }

    /**
     * @return TokenRevokeRequest
     */
    public function getTokenRevokeRequest()
    {
      return $this->tokenRevokeRequest;
    }

    /**
     * @param TokenRevokeRequest $tokenRevokeRequest
     * @return \GpWebpay\WsApi\processTokenRevoke
     */
    public function setTokenRevokeRequest($tokenRevokeRequest)
    {
      $this->tokenRevokeRequest = $tokenRevokeRequest;
      return $this;
    }

}
