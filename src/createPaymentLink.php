<?php

namespace GpWebpay\WsApi;

class createPaymentLink
{

    /**
     * @var PaymentLinkRequest $paymentLinkRequest
     */
    protected $paymentLinkRequest = null;

    /**
     * @param PaymentLinkRequest $paymentLinkRequest
     */
    public function __construct($paymentLinkRequest)
    {
      $this->paymentLinkRequest = $paymentLinkRequest;
    }

    /**
     * @return PaymentLinkRequest
     */
    public function getPaymentLinkRequest()
    {
      return $this->paymentLinkRequest;
    }

    /**
     * @param PaymentLinkRequest $paymentLinkRequest
     * @return \GpWebpay\WsApi\createPaymentLink
     */
    public function setPaymentLinkRequest($paymentLinkRequest)
    {
      $this->paymentLinkRequest = $paymentLinkRequest;
      return $this;
    }

}
