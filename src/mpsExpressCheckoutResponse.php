<?php

namespace GpWebpay\WsApi;

class mpsExpressCheckoutResponse
{

    /**
     * @var MpsExpressCheckoutResponse $mpsExpressCheckoutResponse
     */
    protected $mpsExpressCheckoutResponse = null;

    /**
     * @param MpsExpressCheckoutResponse $mpsExpressCheckoutResponse
     */
    public function __construct($mpsExpressCheckoutResponse)
    {
      $this->mpsExpressCheckoutResponse = $mpsExpressCheckoutResponse;
    }

    /**
     * @return MpsExpressCheckoutResponse
     */
    public function getMpsExpressCheckoutResponse()
    {
      return $this->mpsExpressCheckoutResponse;
    }

    /**
     * @param MpsExpressCheckoutResponse $mpsExpressCheckoutResponse
     * @return \GpWebpay\WsApi\mpsExpressCheckoutResponse
     */
    public function setMpsExpressCheckoutResponse($mpsExpressCheckoutResponse)
    {
      $this->mpsExpressCheckoutResponse = $mpsExpressCheckoutResponse;
      return $this;
    }

}
