<?php

namespace GpWebpay\WsApi;

class processCardOnFilePayment
{

    /**
     * @var CardOnFilePaymentRequest $cardOnFilePaymentRequest
     */
    protected $cardOnFilePaymentRequest = null;

    /**
     * @param CardOnFilePaymentRequest $cardOnFilePaymentRequest
     */
    public function __construct($cardOnFilePaymentRequest)
    {
      $this->cardOnFilePaymentRequest = $cardOnFilePaymentRequest;
    }

    /**
     * @return CardOnFilePaymentRequest
     */
    public function getCardOnFilePaymentRequest()
    {
      return $this->cardOnFilePaymentRequest;
    }

    /**
     * @param CardOnFilePaymentRequest $cardOnFilePaymentRequest
     * @return \GpWebpay\WsApi\processCardOnFilePayment
     */
    public function setCardOnFilePaymentRequest($cardOnFilePaymentRequest)
    {
      $this->cardOnFilePaymentRequest = $cardOnFilePaymentRequest;
      return $this;
    }

}
