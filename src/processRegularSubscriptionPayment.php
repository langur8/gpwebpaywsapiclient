<?php

namespace GpWebpay\WsApi;

class processRegularSubscriptionPayment
{

    /**
     * @var RegularSubscriptionPaymentRequest $regularSubscriptionPaymentRequest
     */
    protected $regularSubscriptionPaymentRequest = null;

    /**
     * @param RegularSubscriptionPaymentRequest $regularSubscriptionPaymentRequest
     */
    public function __construct($regularSubscriptionPaymentRequest)
    {
      $this->regularSubscriptionPaymentRequest = $regularSubscriptionPaymentRequest;
    }

    /**
     * @return RegularSubscriptionPaymentRequest
     */
    public function getRegularSubscriptionPaymentRequest()
    {
      return $this->regularSubscriptionPaymentRequest;
    }

    /**
     * @param RegularSubscriptionPaymentRequest $regularSubscriptionPaymentRequest
     * @return \GpWebpay\WsApi\processRegularSubscriptionPayment
     */
    public function setRegularSubscriptionPaymentRequest($regularSubscriptionPaymentRequest)
    {
      $this->regularSubscriptionPaymentRequest = $regularSubscriptionPaymentRequest;
      return $this;
    }

}
