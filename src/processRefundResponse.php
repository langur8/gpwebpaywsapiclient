<?php

namespace GpWebpay\WsApi;

class processRefundResponse
{

    /**
     * @var RefundResponse $refundRequestResponse
     */
    protected $refundRequestResponse = null;

    /**
     * @param RefundResponse $refundRequestResponse
     */
    public function __construct($refundRequestResponse)
    {
      $this->refundRequestResponse = $refundRequestResponse;
    }

    /**
     * @return RefundResponse
     */
    public function getRefundRequestResponse()
    {
      return $this->refundRequestResponse;
    }

    /**
     * @param RefundResponse $refundRequestResponse
     * @return \GpWebpay\WsApi\processRefundResponse
     */
    public function setRefundRequestResponse($refundRequestResponse)
    {
      $this->refundRequestResponse = $refundRequestResponse;
      return $this;
    }

}
