<?php

namespace GpWebpay\WsApi;

class mpsExpressCheckout
{

    /**
     * @var MpsExpressCheckoutRequest $mpsExpressCheckoutRequest
     */
    protected $mpsExpressCheckoutRequest = null;

    /**
     * @param MpsExpressCheckoutRequest $mpsExpressCheckoutRequest
     */
    public function __construct($mpsExpressCheckoutRequest)
    {
      $this->mpsExpressCheckoutRequest = $mpsExpressCheckoutRequest;
    }

    /**
     * @return MpsExpressCheckoutRequest
     */
    public function getMpsExpressCheckoutRequest()
    {
      return $this->mpsExpressCheckoutRequest;
    }

    /**
     * @param MpsExpressCheckoutRequest $mpsExpressCheckoutRequest
     * @return \GpWebpay\WsApi\mpsExpressCheckout
     */
    public function setMpsExpressCheckoutRequest($mpsExpressCheckoutRequest)
    {
      $this->mpsExpressCheckoutRequest = $mpsExpressCheckoutRequest;
      return $this;
    }

}
