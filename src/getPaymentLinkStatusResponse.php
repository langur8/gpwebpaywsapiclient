<?php

namespace GpWebpay\WsApi;

class getPaymentLinkStatusResponse
{

    /**
     * @var PaymentLinkStatusResponse $paymentLinkStatusResponse
     */
    protected $paymentLinkStatusResponse = null;

    /**
     * @param PaymentLinkStatusResponse $paymentLinkStatusResponse
     */
    public function __construct($paymentLinkStatusResponse)
    {
      $this->paymentLinkStatusResponse = $paymentLinkStatusResponse;
    }

    /**
     * @return PaymentLinkStatusResponse
     */
    public function getPaymentLinkStatusResponse()
    {
      return $this->paymentLinkStatusResponse;
    }

    /**
     * @param PaymentLinkStatusResponse $paymentLinkStatusResponse
     * @return \GpWebpay\WsApi\getPaymentLinkStatusResponse
     */
    public function setPaymentLinkStatusResponse($paymentLinkStatusResponse)
    {
      $this->paymentLinkStatusResponse = $paymentLinkStatusResponse;
      return $this;
    }

}
