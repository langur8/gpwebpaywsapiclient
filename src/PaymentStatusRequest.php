<?php

namespace GpWebpay\WsApi;

class PaymentStatusRequest
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var Provider $provider
     */
    protected $provider = null;

    /**
     * @var MerchantNumber $merchantNumber
     */
    protected $merchantNumber = null;

    /**
     * @var PaymentNumber $paymentNumber
     */
    protected $paymentNumber = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param Provider $provider
     * @param MerchantNumber $merchantNumber
     * @param PaymentNumber $paymentNumber
     * @param Signature $signature
     */
    public function __construct($messageId, $provider, $merchantNumber, $paymentNumber, $signature)
    {
      $this->messageId = $messageId;
      $this->provider = $provider;
      $this->merchantNumber = $merchantNumber;
      $this->paymentNumber = $paymentNumber;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\PaymentStatusRequest
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
      return $this->provider;
    }

    /**
     * @param Provider $provider
     * @return \GpWebpay\WsApi\PaymentStatusRequest
     */
    public function setProvider($provider)
    {
      $this->provider = $provider;
      return $this;
    }

    /**
     * @return MerchantNumber
     */
    public function getMerchantNumber()
    {
      return $this->merchantNumber;
    }

    /**
     * @param MerchantNumber $merchantNumber
     * @return \GpWebpay\WsApi\PaymentStatusRequest
     */
    public function setMerchantNumber($merchantNumber)
    {
      $this->merchantNumber = $merchantNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPaymentNumber()
    {
      return $this->paymentNumber;
    }

    /**
     * @param PaymentNumber $paymentNumber
     * @return \GpWebpay\WsApi\PaymentStatusRequest
     */
    public function setPaymentNumber($paymentNumber)
    {
      $this->paymentNumber = $paymentNumber;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\PaymentStatusRequest
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
