<?php

namespace GpWebpay\WsApi;

class MpsAddress
{

    /**
     * @var string $city
     */
    protected $city = null;

    /**
     * @var string $country
     */
    protected $country = null;

    /**
     * @var string $countrySubdivision
     */
    protected $countrySubdivision = null;

    /**
     * @var string $address1
     */
    protected $address1 = null;

    /**
     * @var string $address2
     */
    protected $address2 = null;

    /**
     * @var string $address3
     */
    protected $address3 = null;

    /**
     * @var string $postalCode
     */
    protected $postalCode = null;

    /**
     * @param string $city
     * @param string $country
     * @param string $address1
     */
    public function __construct($city, $country, $address1)
    {
      $this->city = $city;
      $this->country = $country;
      $this->address1 = $address1;
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param string $country
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountrySubdivision()
    {
      return $this->countrySubdivision;
    }

    /**
     * @param string $countrySubdivision
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setCountrySubdivision($countrySubdivision)
    {
      $this->countrySubdivision = $countrySubdivision;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
      return $this->address1;
    }

    /**
     * @param string $address1
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setAddress1($address1)
    {
      $this->address1 = $address1;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
      return $this->address2;
    }

    /**
     * @param string $address2
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setAddress2($address2)
    {
      $this->address2 = $address2;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddress3()
    {
      return $this->address3;
    }

    /**
     * @param string $address3
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setAddress3($address3)
    {
      $this->address3 = $address3;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
      return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return \GpWebpay\WsApi\MpsAddress
     */
    public function setPostalCode($postalCode)
    {
      $this->postalCode = $postalCode;
      return $this;
    }

}
