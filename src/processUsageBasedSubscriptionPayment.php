<?php

namespace GpWebpay\WsApi;

class processUsageBasedSubscriptionPayment
{

    /**
     * @var UsageBasedSubscriptionPaymentRequest $usageBasedSubscriptionPaymentRequest
     */
    protected $usageBasedSubscriptionPaymentRequest = null;

    /**
     * @param UsageBasedSubscriptionPaymentRequest $usageBasedSubscriptionPaymentRequest
     */
    public function __construct($usageBasedSubscriptionPaymentRequest)
    {
      $this->usageBasedSubscriptionPaymentRequest = $usageBasedSubscriptionPaymentRequest;
    }

    /**
     * @return UsageBasedSubscriptionPaymentRequest
     */
    public function getUsageBasedSubscriptionPaymentRequest()
    {
      return $this->usageBasedSubscriptionPaymentRequest;
    }

    /**
     * @param UsageBasedSubscriptionPaymentRequest $usageBasedSubscriptionPaymentRequest
     * @return \GpWebpay\WsApi\processUsageBasedSubscriptionPayment
     */
    public function setUsageBasedSubscriptionPaymentRequest($usageBasedSubscriptionPaymentRequest)
    {
      $this->usageBasedSubscriptionPaymentRequest = $usageBasedSubscriptionPaymentRequest;
      return $this;
    }

}
