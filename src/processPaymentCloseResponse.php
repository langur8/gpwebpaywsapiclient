<?php

namespace GpWebpay\WsApi;

class processPaymentCloseResponse
{

    /**
     * @var PaymentCloseResponse $paymentCloseResponse
     */
    protected $paymentCloseResponse = null;

    /**
     * @param PaymentCloseResponse $paymentCloseResponse
     */
    public function __construct($paymentCloseResponse)
    {
      $this->paymentCloseResponse = $paymentCloseResponse;
    }

    /**
     * @return PaymentCloseResponse
     */
    public function getPaymentCloseResponse()
    {
      return $this->paymentCloseResponse;
    }

    /**
     * @param PaymentCloseResponse $paymentCloseResponse
     * @return \GpWebpay\WsApi\processPaymentCloseResponse
     */
    public function setPaymentCloseResponse($paymentCloseResponse)
    {
      $this->paymentCloseResponse = $paymentCloseResponse;
      return $this;
    }

}
