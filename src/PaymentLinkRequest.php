<?php

namespace GpWebpay\WsApi;

class PaymentLinkRequest
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var Provider $provider
     */
    protected $provider = null;

    /**
     * @var MerchantNumber $merchantNumber
     */
    protected $merchantNumber = null;

    /**
     * @var PaymentNumber $paymentNumber
     */
    protected $paymentNumber = null;

    /**
     * @var Amount $amount
     */
    protected $amount = null;

    /**
     * @var CurrencyCode $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var CaptureFlag $captureFlag
     */
    protected $captureFlag = null;

    /**
     * @var OrderNumber $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var ReferenceNumber $referenceNumber
     */
    protected $referenceNumber = null;

    /**
     * @var Url $url
     */
    protected $url = null;

    /**
     * @var Description $description
     */
    protected $description = null;

    /**
     * @var MerchantData $merchantData
     */
    protected $merchantData = null;

    /**
     * @var PaymentNumber $fastPayId
     */
    protected $fastPayId = null;

    /**
     * @var PayMethod $defaultPayMethod
     */
    protected $defaultPayMethod = null;

    /**
     * @var PayMethod $disabledPayMethods
     */
    protected $disabledPayMethods = null;

    /**
     * @var Email $email
     */
    protected $email = null;

    /**
     * @var Email $merchantEmail
     */
    protected $merchantEmail = null;

    /**
     * @var date $paymentExpiry
     */
    protected $paymentExpiry = null;

    /**
     * @var Language $language
     */
    protected $language = null;

    /**
     * @var BooleanType $registerRecurring
     */
    protected $registerRecurring = null;

    /**
     * @var BooleanType $registerToken
     */
    protected $registerToken = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param Provider $provider
     * @param MerchantNumber $merchantNumber
     * @param PaymentNumber $paymentNumber
     * @param Amount $amount
     * @param CurrencyCode $currencyCode
     * @param CaptureFlag $captureFlag
     * @param date $paymentExpiry
     * @param Signature $signature
     */
    public function __construct($messageId, $provider, $merchantNumber, $paymentNumber, $amount, $currencyCode, $captureFlag, $paymentExpiry, $signature)
    {
      $this->messageId = $messageId;
      $this->provider = $provider;
      $this->merchantNumber = $merchantNumber;
      $this->paymentNumber = $paymentNumber;
      $this->amount = $amount;
      $this->currencyCode = $currencyCode;
      $this->captureFlag = $captureFlag;
      $this->paymentExpiry = $paymentExpiry;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
      return $this->provider;
    }

    /**
     * @param Provider $provider
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setProvider($provider)
    {
      $this->provider = $provider;
      return $this;
    }

    /**
     * @return MerchantNumber
     */
    public function getMerchantNumber()
    {
      return $this->merchantNumber;
    }

    /**
     * @param MerchantNumber $merchantNumber
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setMerchantNumber($merchantNumber)
    {
      $this->merchantNumber = $merchantNumber;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPaymentNumber()
    {
      return $this->paymentNumber;
    }

    /**
     * @param PaymentNumber $paymentNumber
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setPaymentNumber($paymentNumber)
    {
      $this->paymentNumber = $paymentNumber;
      return $this;
    }

    /**
     * @return Amount
     */
    public function getAmount()
    {
      return $this->amount;
    }

    /**
     * @param Amount $amount
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setAmount($amount)
    {
      $this->amount = $amount;
      return $this;
    }

    /**
     * @return CurrencyCode
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param CurrencyCode $currencyCode
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return CaptureFlag
     */
    public function getCaptureFlag()
    {
      return $this->captureFlag;
    }

    /**
     * @param CaptureFlag $captureFlag
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setCaptureFlag($captureFlag)
    {
      $this->captureFlag = $captureFlag;
      return $this;
    }

    /**
     * @return OrderNumber
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param OrderNumber $orderNumber
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return ReferenceNumber
     */
    public function getReferenceNumber()
    {
      return $this->referenceNumber;
    }

    /**
     * @param ReferenceNumber $referenceNumber
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setReferenceNumber($referenceNumber)
    {
      $this->referenceNumber = $referenceNumber;
      return $this;
    }

    /**
     * @return Url
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param Url $url
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * @return Description
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param Description $description
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

    /**
     * @return MerchantData
     */
    public function getMerchantData()
    {
      return $this->merchantData;
    }

    /**
     * @param MerchantData $merchantData
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setMerchantData($merchantData)
    {
      $this->merchantData = $merchantData;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getFastPayId()
    {
      return $this->fastPayId;
    }

    /**
     * @param PaymentNumber $fastPayId
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setFastPayId($fastPayId)
    {
      $this->fastPayId = $fastPayId;
      return $this;
    }

    /**
     * @return PayMethod
     */
    public function getDefaultPayMethod()
    {
      return $this->defaultPayMethod;
    }

    /**
     * @param PayMethod $defaultPayMethod
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setDefaultPayMethod($defaultPayMethod)
    {
      $this->defaultPayMethod = $defaultPayMethod;
      return $this;
    }

    /**
     * @return PayMethod
     */
    public function getDisabledPayMethods()
    {
      return $this->disabledPayMethods;
    }

    /**
     * @param PayMethod $disabledPayMethods
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setDisabledPayMethods($disabledPayMethods)
    {
      $this->disabledPayMethods = $disabledPayMethods;
      return $this;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param Email $email
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return Email
     */
    public function getMerchantEmail()
    {
      return $this->merchantEmail;
    }

    /**
     * @param Email $merchantEmail
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setMerchantEmail($merchantEmail)
    {
      $this->merchantEmail = $merchantEmail;
      return $this;
    }

    /**
     * @return date
     */
    public function getPaymentExpiry()
    {
      return $this->paymentExpiry;
    }

    /**
     * @param date $paymentExpiry
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setPaymentExpiry($paymentExpiry)
    {
      $this->paymentExpiry = $paymentExpiry;
      return $this;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
      return $this->language;
    }

    /**
     * @param Language $language
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setLanguage($language)
    {
      $this->language = $language;
      return $this;
    }

    /**
     * @return BooleanType
     */
    public function getRegisterRecurring()
    {
      return $this->registerRecurring;
    }

    /**
     * @param BooleanType $registerRecurring
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setRegisterRecurring($registerRecurring)
    {
      $this->registerRecurring = $registerRecurring;
      return $this;
    }

    /**
     * @return BooleanType
     */
    public function getRegisterToken()
    {
      return $this->registerToken;
    }

    /**
     * @param BooleanType $registerToken
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setRegisterToken($registerToken)
    {
      $this->registerToken = $registerToken;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\PaymentLinkRequest
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
