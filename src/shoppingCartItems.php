<?php

namespace GpWebpay\WsApi;

class shoppingCartItems
{

    /**
     * @var ShoppingCartItem $shoppingCartItem
     */
    protected $shoppingCartItem = null;

    /**
     * @param ShoppingCartItem $shoppingCartItem
     */
    public function __construct($shoppingCartItem)
    {
      $this->shoppingCartItem = $shoppingCartItem;
    }

    /**
     * @return ShoppingCartItem
     */
    public function getShoppingCartItem()
    {
      return $this->shoppingCartItem;
    }

    /**
     * @param ShoppingCartItem $shoppingCartItem
     * @return \GpWebpay\WsApi\shoppingCartItems
     */
    public function setShoppingCartItem($shoppingCartItem)
    {
      $this->shoppingCartItem = $shoppingCartItem;
      return $this;
    }

}
