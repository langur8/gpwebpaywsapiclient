<?php

namespace GpWebpay\WsApi;

class cardDetail
{

    /**
     * @var textValue $brandId
     */
    protected $brandId = null;

    /**
     * @var textValue $brandName
     */
    protected $brandName = null;

    /**
     * @var textValue $cardHolderName
     */
    protected $cardHolderName = null;

    /**
     * @var month $expiryMonth
     */
    protected $expiryMonth = null;

    /**
     * @var year $expiryYear
     */
    protected $expiryYear = null;

    /**
     * @var textValue $cardId
     */
    protected $cardId = null;

    /**
     * @var textValue $lastFour
     */
    protected $lastFour = null;

    /**
     * @var textValue $cardAlias
     */
    protected $cardAlias = null;

    /**
     * @param textValue $brandId
     * @param textValue $brandName
     * @param textValue $cardHolderName
     * @param month $expiryMonth
     * @param year $expiryYear
     * @param textValue $cardId
     * @param textValue $lastFour
     * @param textValue $cardAlias
     */
    public function __construct($brandId, $brandName, $cardHolderName, $expiryMonth, $expiryYear, $cardId, $lastFour, $cardAlias)
    {
      $this->brandId = $brandId;
      $this->brandName = $brandName;
      $this->cardHolderName = $cardHolderName;
      $this->expiryMonth = $expiryMonth;
      $this->expiryYear = $expiryYear;
      $this->cardId = $cardId;
      $this->lastFour = $lastFour;
      $this->cardAlias = $cardAlias;
    }

    /**
     * @return textValue
     */
    public function getBrandId()
    {
      return $this->brandId;
    }

    /**
     * @param textValue $brandId
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setBrandId($brandId)
    {
      $this->brandId = $brandId;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getBrandName()
    {
      return $this->brandName;
    }

    /**
     * @param textValue $brandName
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setBrandName($brandName)
    {
      $this->brandName = $brandName;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getCardHolderName()
    {
      return $this->cardHolderName;
    }

    /**
     * @param textValue $cardHolderName
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setCardHolderName($cardHolderName)
    {
      $this->cardHolderName = $cardHolderName;
      return $this;
    }

    /**
     * @return month
     */
    public function getExpiryMonth()
    {
      return $this->expiryMonth;
    }

    /**
     * @param month $expiryMonth
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setExpiryMonth($expiryMonth)
    {
      $this->expiryMonth = $expiryMonth;
      return $this;
    }

    /**
     * @return year
     */
    public function getExpiryYear()
    {
      return $this->expiryYear;
    }

    /**
     * @param year $expiryYear
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setExpiryYear($expiryYear)
    {
      $this->expiryYear = $expiryYear;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getCardId()
    {
      return $this->cardId;
    }

    /**
     * @param textValue $cardId
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setCardId($cardId)
    {
      $this->cardId = $cardId;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getLastFour()
    {
      return $this->lastFour;
    }

    /**
     * @param textValue $lastFour
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setLastFour($lastFour)
    {
      $this->lastFour = $lastFour;
      return $this;
    }

    /**
     * @return textValue
     */
    public function getCardAlias()
    {
      return $this->cardAlias;
    }

    /**
     * @param textValue $cardAlias
     * @return \GpWebpay\WsApi\cardDetail
     */
    public function setCardAlias($cardAlias)
    {
      $this->cardAlias = $cardAlias;
      return $this;
    }

}
