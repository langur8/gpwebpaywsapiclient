<?php

namespace GpWebpay\WsApi;

class PaymentLinkResponse
{

    /**
     * @var MessageId $messageId
     */
    protected $messageId = null;

    /**
     * @var PaymentNumber $paymentNumber
     */
    protected $paymentNumber = null;

    /**
     * @var string $paymentLink
     */
    protected $paymentLink = null;

    /**
     * @var Signature $signature
     */
    protected $signature = null;

    /**
     * @param MessageId $messageId
     * @param PaymentNumber $paymentNumber
     * @param string $paymentLink
     * @param Signature $signature
     */
    public function __construct($messageId, $paymentNumber, $paymentLink, $signature)
    {
      $this->messageId = $messageId;
      $this->paymentNumber = $paymentNumber;
      $this->paymentLink = $paymentLink;
      $this->signature = $signature;
    }

    /**
     * @return MessageId
     */
    public function getMessageId()
    {
      return $this->messageId;
    }

    /**
     * @param MessageId $messageId
     * @return \GpWebpay\WsApi\PaymentLinkResponse
     */
    public function setMessageId($messageId)
    {
      $this->messageId = $messageId;
      return $this;
    }

    /**
     * @return PaymentNumber
     */
    public function getPaymentNumber()
    {
      return $this->paymentNumber;
    }

    /**
     * @param PaymentNumber $paymentNumber
     * @return \GpWebpay\WsApi\PaymentLinkResponse
     */
    public function setPaymentNumber($paymentNumber)
    {
      $this->paymentNumber = $paymentNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentLink()
    {
      return $this->paymentLink;
    }

    /**
     * @param string $paymentLink
     * @return \GpWebpay\WsApi\PaymentLinkResponse
     */
    public function setPaymentLink($paymentLink)
    {
      $this->paymentLink = $paymentLink;
      return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
      return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return \GpWebpay\WsApi\PaymentLinkResponse
     */
    public function setSignature($signature)
    {
      $this->signature = $signature;
      return $this;
    }

}
